﻿#if UNITY_IOS
using System.IO;
using Facebook.Unity.Settings;
using Unity.VisualScripting;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

namespace Editor
{
    public class Postprocessor
    {
            [PostProcessBuild]
            public static void OnPostprocessorBuild(BuildTarget buildTarget, string path)
            {
                string plistPath = path + "/Info.plist";
                PlistDocument plist = new PlistDocument();
                plist.ReadFromString(File.ReadAllText(plistPath));
                PlistElementDict rootDict = plist.root;
                rootDict.SetString("FacebookClientToken", FacebookSettings.ClientTokens[0]);
                File.WriteAllText(plistPath, plist.WriteToString());
            }

    }
}
#endif