﻿namespace Gameplay.Stack.Item.Utils
{
    public interface IItemAnimationEventDisappear
    {
        void Disappeared();
    }
}