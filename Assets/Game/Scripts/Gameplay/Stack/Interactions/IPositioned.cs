﻿using UnityEngine;

namespace Gameplay
{
    public interface IPositioned
    {
        public Vector3 Position { get; }
    }
}