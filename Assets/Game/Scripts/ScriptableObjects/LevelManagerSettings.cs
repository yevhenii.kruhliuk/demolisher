﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "New LM Settings", menuName = "ScriptableObjects/Create New LM Setting")]
    public class LevelManagerSettings : ScriptableObject
    {
        public List<string> levels;
        public Vector2Int levelsRange;
    }
}

