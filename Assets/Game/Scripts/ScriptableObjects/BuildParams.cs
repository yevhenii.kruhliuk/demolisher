﻿using Configs.Items;
using UnityEngine;

namespace Game.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "BuildParam", menuName = "ScriptableObjects/BuildParam")]
    public class BuildParams : ScriptableObject
    {
        public float BuildPartHP;
        public int ItemsCount;
        public ItemConfigBase ItemConfig;
    }
}