﻿using UnityEngine;

namespace Game.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "DemolisherStartParams", menuName = "ScriptableObjects/DemolisherStartParams")]
    public class DemolisherStartParams : ScriptableObject
    {
        public float StartSpeed;
        public float StartDamage;
        public int StartMaxStack;
        public float BodyRotateTime;
    }
}