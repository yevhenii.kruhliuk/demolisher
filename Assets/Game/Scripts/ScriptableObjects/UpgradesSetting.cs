﻿using Configs.Items;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "UpgradesSetting", menuName = "ScriptableObjects/UpgradesSetting")]
    public class UpgradesSetting : ScriptableObject
    {
        [BoxGroup("Start Prices")]public int StartSpeedUpgradePrice;
        [BoxGroup("Start Prices")]public int StartDamageUpgradePrice;
        [BoxGroup("Start Prices")]public int StartMaxStackUpgradePrice;
        [BoxGroup("Start Prices")]public int StartBodyRotateTimeUpgradePrice;

        [BoxGroup("Upgrades Values")] public float SpeedIncreasePerLevel;
        [BoxGroup("Upgrades Values")] public float DamageIncreasePerLevel;
        [BoxGroup("Upgrades Values")] public int StackIncreasePerLevel;
        [BoxGroup("Upgrades Values")] public float BodyIncreasePerLevel;
        
        public int PriceMultipler;

        public int MoneyPerOneStone;
    }
}