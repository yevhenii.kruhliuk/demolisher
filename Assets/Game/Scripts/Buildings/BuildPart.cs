﻿using System;
using System.Collections.Generic;
using Game.Scripts.Core;
using Game.Scripts.UI;
using UnityEngine;

namespace Game.Scripts.Buildings
{
    public class BuildPart : MonoBehaviour
    {
        public int BuildPartLevel;
        public float HP;
        private GameObject _slicedObject;
        public bool _isDestroyed;
        [SerializeField] private GameObject _objectToOff;
        [SerializeField] private bool _isCollone;
        [SerializeField] private List<BuildPart> _connettedWalls;

        private void Start()
        {
            HP = DataStorage.Instance.BuildingParams[BuildPartLevel].BuildPartHP;
            _slicedObject = transform.parent.GetChild(1).gameObject;
            _slicedObject.SetActive(false);
            _isDestroyed = false;
            UIManager.Instance.InGameMenu.BuildPartsCount += 1;
        }

        private void Update()
        {
            if (_isCollone)
            {
                if (_connettedWalls[0]._isDestroyed && _connettedWalls[1]._isDestroyed)
                {
                    HP = 0;
                }
            }
            if (HP <= 0)
            {
                OnDestroyEvent();
                if(_objectToOff)
                    _objectToOff.SetActive(false);
                gameObject.SetActive(false);
                _slicedObject.SetActive(true);
            }
        }

        private void OnDestroyEvent()
        {
            if (UIManager.Instance != null)
            {
                UIManager.Instance.InGameMenu.BrokenBuildPartsCount += 1;
                for (int i = 0; i < DataStorage.Instance.BuildingParams[BuildPartLevel].ItemsCount; i++)
                {
                    var item = GameManager.Instance.ItemsPool.Get(transform.position);
                    item.Load(DataStorage.Instance.BuildingParams[BuildPartLevel].ItemConfig);
                    //item.AddExplosionForce();
                    GameManager.Instance.unreceiveItems.Push(item);
                }
                _isDestroyed = true;
                enabled = false;
            }
        }
    }
}