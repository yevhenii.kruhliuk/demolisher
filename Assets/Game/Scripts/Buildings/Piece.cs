﻿using System;
using System.Collections;
using UnityEngine;

namespace Game.Scripts.Buildings
{
    public class Piece : MonoBehaviour
    {
        private BoxCollider _collider;
        //private MeshRenderer _meshRenderer;
        private void OnEnable()
        {
            _collider = GetComponent<BoxCollider>();
            StartCoroutine(SetColliderDisableWithDelay(3f, 1.5f));
        }

        private IEnumerator SetColliderDisableWithDelay(float time, float offTime)
        {
            yield return new WaitForSecondsRealtime(time);
            _collider.enabled = false;
            StartCoroutine(SetObjectDisableWithDelay(offTime));
        }

        private IEnumerator SetObjectDisableWithDelay(float time)
        {
            yield return new WaitForSecondsRealtime(time);
            gameObject.SetActive(false);
        }
    }
}