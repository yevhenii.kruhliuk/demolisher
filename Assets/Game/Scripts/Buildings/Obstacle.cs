﻿using System;
using System.Collections.Generic;
using Game.Scripts.Stucts;
using UnityEngine;

namespace Game.Scripts.Buildings
{
    public class Obstacle : MonoBehaviour
    {
        [SerializeField] private bool _isSpecific;
        private List<Rigidbody> _rigidbodies;
        private List<ObstaclePiece> _obstaclePieces;
        private BoxCollider _boxCollider;
        private void Awake()
        {
            _rigidbodies = new List<Rigidbody>();
            _obstaclePieces = new List<ObstaclePiece>();
            _rigidbodies.Add(GetComponent<Rigidbody>());
            _obstaclePieces.Add(GetComponent<ObstaclePiece>());
            for (int i = 0; i < transform.childCount; i++)
            {
                _rigidbodies.Add(transform.GetChild(i).GetComponent<Rigidbody>());
                _obstaclePieces.Add(transform.GetChild(i).GetComponent<ObstaclePiece>());
            }

            if (_isSpecific)
            {
                _boxCollider = GetComponent<BoxCollider>();
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag(Tags.Player) || collision.gameObject.CompareTag(Tags.Ball))
            {
                foreach (var rb in _rigidbodies)
                {
                    rb.isKinematic = false;
                    if (_isSpecific)
                        _boxCollider.enabled = false;
                }

                foreach (var piece in _obstaclePieces)
                {
                    piece.Inhumation();
                }
            }
        }
    }
}