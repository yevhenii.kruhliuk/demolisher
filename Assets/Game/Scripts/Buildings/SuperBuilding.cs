﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Game.Scripts.Core;
using Game.Scripts.UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Scripts.Buildings
{
    public class SuperBuilding : MonoBehaviour
    {
        public float HP;
        public int ItemCount;
        private bool _isDestroyed;
        private MeshRenderer _meshRenderer;
        private BoxCollider _boxCollider;
        private List<GameObject> _pieces;

        [SerializeField] private Transform _piecesParent;
        [SerializeField] private ParticleSystem _hitVFX;
        [SerializeField] private ParticleSystem _hitVFX2;

        private void Awake()
        {
            _meshRenderer = GetComponent<MeshRenderer>();
            _boxCollider = GetComponent<BoxCollider>();
            _pieces = new List<GameObject>();
            for (int i = 0; i < _piecesParent.childCount; i++)
            {
                _pieces.Add(_piecesParent.GetChild(i).gameObject);
            }
        }

        private void Start()
        {
            UIManager.Instance.InGameMenu.BuildPartsCount += 1;
            _isDestroyed = false;
        }
        
        private void Update()
        {
            if (HP <= 0)
            {
                OnDestroyEvent();
                _meshRenderer.enabled = false;
                _boxCollider.enabled = false;
                foreach (var piece in _pieces)
                {
                    piece.SetActive(true);
                }
                if(_hitVFX != null)
                    _hitVFX.Play();
                if(_hitVFX2 != null)
                    _hitVFX2.Play();
            }
        }
        
        private void OnDestroyEvent()
        {
            if (UIManager.Instance != null)
            {
                UIManager.Instance.InGameMenu.BrokenBuildPartsCount += 1;
                for (int i = 0; i < ItemCount; i++)
                {
                    //Vector3 spawnPos = new Vector3(randX, randY, randZ);
                    var item = GameManager.Instance.ItemsPool.Get(transform.position);
                    item.Load(DataStorage.Instance.BuildingParams[0].ItemConfig);
                    GameManager.Instance.unreceiveItems.Push(item);
                }
                _isDestroyed = true;
                enabled = false;
            }
        }

        public void ShakeBuild()
        {
            Vector3 strenght = new Vector3(1f, 0f, 1f);
            transform.DOShakePosition(1f, strenght, 10,90f,false, true);
        }
    }
}