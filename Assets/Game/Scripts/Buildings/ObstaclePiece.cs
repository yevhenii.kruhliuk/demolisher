﻿using System;
using System.Collections;
using UnityEngine;

namespace Game.Scripts.Buildings
{
    public class ObstaclePiece : MonoBehaviour
    {
        private MeshCollider _collider;

        private void Awake()
        {
            _collider = GetComponent<MeshCollider>();
        }

        public void Inhumation()
        {
            StartCoroutine(SetColliderDisableWithDelay(2f, 1f));
        }
        
        private IEnumerator SetColliderDisableWithDelay(float time, float offTime)
        {
            yield return new WaitForSecondsRealtime(time);
            _collider.enabled = false;
            StartCoroutine(SetObjectDisableWithDelay(offTime));
        }

        private IEnumerator SetObjectDisableWithDelay(float time)
        {
            yield return new WaitForSecondsRealtime(time);
            gameObject.SetActive(false);
        }
    }
}