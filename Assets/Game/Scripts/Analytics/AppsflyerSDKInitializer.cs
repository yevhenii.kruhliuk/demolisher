﻿using System;
using AppsFlyerSDK;
using UnityEngine;

namespace Game.Scripts.Analytics
{
    public class AppsflyerSDKInitializer : MonoBehaviour
    {
        private void Awake()
        {
            AppsFlyer.initSDK("BDt9xS3kmRET5qbDQgEMYk", "appID");
            AppsFlyer.startSDK();
            DontDestroyOnLoad(gameObject);
        }
    }
}