using UnityEngine;

namespace Game.Scripts.Analytic
{
    public class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        private static T instance;
        public static T Instance
        {
            get
            {
                return instance;
            }
            
            protected set
            {
                instance = value;
            }
        }

        public static bool IsInitialized()
        {
            return instance != null;
        }

        protected virtual void Awake()
        {
            if (instance == null)
            {
                instance = (T)this;
            }
            else
            {
                Debug.LogError("[Singleton] trying to instantiate a second instance of singleton class.");
            }
        }


        protected virtual void Start()
        {
            DontDestroyOnLoad(gameObject);
        }

        protected virtual void OnDestroy()
        {
            if (instance == this)
            {
                instance = null;
            }
        }
    }
}
