using System.Collections.Generic;
using Facebook.Unity;

namespace Game.Scripts.Analytic
{
    public class FacebookManager : Singleton<FacebookManager>
    {
        #if !UNITY_EDITOR
            protected override void Awake()
            {
                base.Awake();
                if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                }
                else
                {
                    FB.Init(() => {
                        FB.ActivateApp();
                    });
                }
            }
            private void OnApplicationPause(bool pauseStatus)
            {
                if (!pauseStatus)
                {
                    if (FB.IsInitialized)
                    {
                        FB.ActivateApp();
                    }
                    else
                    {
                        FB.Init(() => {
                            FB.ActivateApp();
                        });
                    }
                }
            }

            public void LogAchieveLevelEvent(string level)
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters[AppEventParameterName.Level] = level;
                FB.LogAppEvent(AppEventName.AchievedLevel, 0, parameters);
            }
        #endif
    }
}
