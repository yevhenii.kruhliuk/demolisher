﻿namespace Common
{
    public interface IInitializable<T>
    {
        void Init(in T value);
    }
    
}