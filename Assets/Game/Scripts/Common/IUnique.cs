﻿namespace Common
{
    public interface IUnique
    {
        uint IntId { get; }
    }
}