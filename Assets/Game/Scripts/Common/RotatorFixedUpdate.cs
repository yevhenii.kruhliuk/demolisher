﻿using UnityEngine;

namespace Common
{
    public class RotatorFixedUpdate : MonoBehaviour
    {
        [SerializeField] private Vector3 eulers;

        private void FixedUpdate()
        {
            transform.Rotate(eulers);
        }
    }
}