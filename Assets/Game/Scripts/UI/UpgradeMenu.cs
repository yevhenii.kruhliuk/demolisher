﻿using System;
using DG.Tweening;
using Game.Scripts.Core;
using Game.Scripts.Stucts;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Game.Scripts.UI
{
    public class UpgradeMenu : MonoBehaviour
    {
        public event Action OnStartMovementEvent;
        public event Action OnStopMovementEvent;
        public event Action OnTutorDoneEvent;

        public event Action<int> OnSpeedUpgradeEvent;
        public event Action<int> OnBallUpgradeEvent;
        public event Action<int> OnBodyUpgradeEvent;
        public event Action OnStackUpgradeEvent;
        
        [BoxGroup("BallUpgrade")] [SerializeField] private TextMeshProUGUI _ballUpgradePriceText;
        [BoxGroup("BallUpgrade")] [SerializeField] private TextMeshProUGUI _ballFromText;
        [BoxGroup("BallUpgrade")] [SerializeField] private TextMeshProUGUI _ballToText;
        [BoxGroup("BallUpgrade")] [SerializeField] private GameObject[] _ballLevelImages;
        [BoxGroup("BallUpgrade")] [SerializeField] private GameObject _maxBallLevel;
        [BoxGroup("BallUpgrade")] [SerializeField] private GameObject _ballUpgradeButtonActive;
        [BoxGroup("BallUpgrade")] [SerializeField] private GameObject _ballUpgradeButtonUnactive;
        
        [BoxGroup("BodyUpgrade")] [SerializeField] private TextMeshProUGUI _bodyUpgradePriceText;
        [BoxGroup("BodyUpgrade")] [SerializeField] private TextMeshProUGUI _bodyFromText;
        [BoxGroup("BodyUpgrade")] [SerializeField] private TextMeshProUGUI _bodyToText;
        [BoxGroup("BodyUpgrade")] [SerializeField] private GameObject[] _bodyLevelImages;
        [BoxGroup("BodyUpgrade")] [SerializeField] private GameObject _maxBodyLevel;
        [BoxGroup("BodyUpgrade")] [SerializeField] private GameObject _bodyUpgradeButtonActive;
        [BoxGroup("BodyUpgrade")] [SerializeField] private GameObject _bodyUpgradeButtonUnactive;
        
        [BoxGroup("StackUpgrade")] [SerializeField] private TextMeshProUGUI _stackUpgradePriceText;
        [BoxGroup("StackUpgrade")] [SerializeField] private TextMeshProUGUI _stackFromText;
        [BoxGroup("StackUpgrade")] [SerializeField] private TextMeshProUGUI _stacktToText;
        [BoxGroup("StackUpgrade")] [SerializeField] private GameObject[] _stackLevelImages;
        [BoxGroup("StackUpgrade")] [SerializeField] private GameObject _maxStackLevel;
        [BoxGroup("StackUpgrade")] [SerializeField] private GameObject _stackUpgradeButtonActive;
        [BoxGroup("StackUpgrade")] [SerializeField] private GameObject _stackUpgradeButtonUnactive;
        
        [BoxGroup("SpeedUpgrade")] [SerializeField] private TextMeshProUGUI _speedUpgradePriceText;
        [BoxGroup("SpeedUpgrade")] [SerializeField] private TextMeshProUGUI _speedFromText;
        [BoxGroup("SpeedUpgrade")] [SerializeField] private TextMeshProUGUI _speedtToText;
        [BoxGroup("SpeedUpgrade")] [SerializeField] private GameObject[] _speedLevelImages;
        [BoxGroup("SpeedUpgrade")] [SerializeField] private GameObject _maxSpeedLevel;
        [BoxGroup("SpeedUpgrade")] [SerializeField] private GameObject _speedUpgradeButtonActive;
        [BoxGroup("SpeedUpgrade")] [SerializeField] private GameObject _speedUpgradeButtonUnactive;

        private int _upgradeSpeedLevel;
        private int _upgradeBallLevel;
        private int _upgradeBodyLevel;
        private int _upgradeStackLevel;

        private int _upgradeSpeedPrice;
        private int _upgradeBallPrice;
        private int _upgradeBodyPrice;
        private int _upgradeStackPrice;
        

        private void Start()
        {
            EventSubscriber();
            LoadSaves();
            InitializeUpgradesMenu();
        }

        private void InitializeUpgradesMenu()
        {
            UpdateButtonStatus();
            SpeedViewInitialize();
            StackViewInitialize();
            BallViewInitialize();
            BodyViewInitialize();
        }
        
        private void SpeedViewInitialize()
        {
            _speedUpgradePriceText.text = _upgradeSpeedPrice.ToString();
            _speedFromText.text = GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.Speed).ToString();
            _speedtToText.text = (GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.Speed) + DataStorage.Instance.UpgradesSetting.SpeedIncreasePerLevel).ToString();
            
            for (int i = 0; i < _upgradeSpeedLevel; i++)
            {
                _speedLevelImages[i].SetActive(true);
            }
            if (_upgradeSpeedLevel == 5)
            {
                _speedFromText.transform.parent.gameObject.SetActive(false);
                _maxSpeedLevel.SetActive(true);
                _speedUpgradeButtonActive.SetActive(false);
                //_speedUpgradeButtonUnactive.SetActive(false);
            }
        }
        
        private void StackViewInitialize()
        {
            _stackUpgradePriceText.text = _upgradeStackPrice.ToString();
            _stackFromText.text = GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.MaxStack).ToString();
            _stacktToText.text = (GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.MaxStack) + DataStorage.Instance.UpgradesSetting.StackIncreasePerLevel).ToString();
            
            for (int i = 0; i < _upgradeStackLevel; i++)
            {
                _stackLevelImages[i].SetActive(true);
            }
            if (_upgradeStackLevel == 5)
            {
                _stackFromText.transform.parent.gameObject.SetActive(false);
                _maxStackLevel.SetActive(true);
                _stackUpgradeButtonActive.SetActive(false);
                //_stackUpgradeButtonUnactive.SetActive(false);
            }
        }
        
        private void BallViewInitialize()
        {
            _ballUpgradePriceText.text = _upgradeBallPrice.ToString();
            _ballFromText.text = GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.Damage).ToString();
            _ballToText.text = (GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.Damage) * DataStorage.Instance.UpgradesSetting.DamageIncreasePerLevel).ToString();
            
            for (int i = 0; i < _upgradeBallLevel; i++)
            {
                _ballLevelImages[i].SetActive(true);
            }
            if (_upgradeBallLevel == 5)
            {
                _ballFromText.transform.parent.gameObject.SetActive(false);
                _maxBallLevel.SetActive(true);
                _ballUpgradeButtonActive.SetActive(false);
                //_ballUpgradeButtonUnactive.SetActive(false);
            }
        }
        
        private void BodyViewInitialize()
        {
            _bodyUpgradePriceText.text = _upgradeBodyPrice.ToString();
            _bodyFromText.text = GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.BodyRotateTime).ToString();
            _bodyToText.text = (GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.BodyRotateTime) - DataStorage.Instance.UpgradesSetting.BodyIncreasePerLevel).ToString();
            
            for (int i = 0; i < _upgradeBodyLevel; i++)
            {
                _bodyLevelImages[i].SetActive(true);
            }
            if (_upgradeBodyLevel == 5)
            {
                _bodyFromText.transform.parent.gameObject.SetActive(false);
                _maxBodyLevel.SetActive(true);
                _bodyUpgradeButtonActive.SetActive(false);
                //_bodyUpgradeButtonUnactive.SetActive(false);
            }
        }
        
        public void UpgradeSpeed()
        {
            if(GameManager.Instance.CashAmount < _upgradeSpeedPrice)
                return;
            if(_upgradeSpeedLevel == 5)
                return;
            if (UIManager.Instance.ItsATutor)
                PassTutorial();
            GameManager.Instance.CashAmount -= _upgradeSpeedPrice;
            _upgradeSpeedPrice *= DataStorage.Instance.UpgradesSetting.PriceMultipler;
            _speedUpgradePriceText.text = _upgradeSpeedPrice.ToString();
            _upgradeSpeedLevel++;
            PlayerPrefs.SetInt(PlayerPrefsKeys.SpeedPrice, _upgradeSpeedPrice);
            PlayerPrefs.SetInt(PlayerPrefsKeys.SpeedLevel, _upgradeSpeedLevel);
            OnSpeedUpgradeEvent?.Invoke(_upgradeSpeedLevel);
            UpdateButtonStatus();
            _speedFromText.text = GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.Speed).ToString();
            _speedtToText.text = (GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.Speed) + DataStorage.Instance.UpgradesSetting.SpeedIncreasePerLevel).ToString();
            for (int i = 0; i < _upgradeSpeedLevel; i++)
            {
                _speedLevelImages[i].SetActive(true);
            }
            if (_upgradeSpeedLevel == 5)
            {
                _speedFromText.transform.parent.gameObject.SetActive(false);
                _maxSpeedLevel.SetActive(true);
                _speedUpgradeButtonActive.SetActive(false);
                //_speedUpgradeButtonUnactive.SetActive(false);
            }
        }
        
        public void UpgradeBall()
        {
            if(GameManager.Instance.CashAmount < _upgradeBallPrice)
                return;
            if(_upgradeBallLevel == 5)
                return;
            if (UIManager.Instance.ItsATutor)
                PassTutorial();
            
            GameManager.Instance.CashAmount -= _upgradeBallPrice;
            _upgradeBallPrice *= DataStorage.Instance.UpgradesSetting.PriceMultipler;
            _ballUpgradePriceText.text = _upgradeBallPrice.ToString();
            _upgradeBallLevel++;
            PlayerPrefs.SetInt(PlayerPrefsKeys.DamagePrice, _upgradeBallPrice);
            PlayerPrefs.SetInt(PlayerPrefsKeys.DamageLevel, _upgradeBallLevel);
            OnBallUpgradeEvent?.Invoke(_upgradeBallLevel);
            UpdateButtonStatus();
            _ballFromText.text = GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.Damage).ToString();
            _ballToText.text = (GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.Damage) * DataStorage.Instance.UpgradesSetting.DamageIncreasePerLevel).ToString();
            for (int i = 0; i < _upgradeBallLevel; i++)
            {
                _ballLevelImages[i].SetActive(true);
            }
            if (_upgradeBallLevel == 5)
            {
                _ballFromText.transform.parent.gameObject.SetActive(false);
                _maxBallLevel.SetActive(true);
                _ballUpgradeButtonActive.SetActive(false);
                //_ballUpgradeButtonUnactive.SetActive(false);
            }
        }
        
        public void UpgradeBody()
        {
            if(GameManager.Instance.CashAmount < _upgradeBodyPrice)
                return;
            if(_upgradeBodyLevel == 5)
                return;
            if (UIManager.Instance.ItsATutor)
                PassTutorial();
            
            GameManager.Instance.CashAmount -= _upgradeBodyPrice;
            _upgradeBodyPrice *= DataStorage.Instance.UpgradesSetting.PriceMultipler;
            _bodyUpgradePriceText.text = _upgradeBodyPrice.ToString();
            _upgradeBodyLevel++;
            PlayerPrefs.SetInt(PlayerPrefsKeys.BodySpeedPrice, _upgradeBodyPrice);
            PlayerPrefs.SetInt(PlayerPrefsKeys.BodySpeedLevel, _upgradeBodyLevel);
            OnBodyUpgradeEvent?.Invoke(_upgradeBodyLevel);
            UpdateButtonStatus();
            _bodyFromText.text = GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.BodyRotateTime).ToString();
            _bodyToText.text = (GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.BodyRotateTime) - DataStorage.Instance.UpgradesSetting.BodyIncreasePerLevel).ToString();
            for (int i = 0; i < _upgradeBodyLevel; i++)
            {
                _bodyLevelImages[i].SetActive(true);
            }
            if (_upgradeBodyLevel == 5)
            {
                _bodyFromText.transform.parent.gameObject.SetActive(false);
                _maxBodyLevel.SetActive(true);
                _bodyUpgradeButtonActive.SetActive(false);
                //_bodyUpgradeButtonUnactive.SetActive(false);
            }
        }
        
        public void UpgradeStack()
        {
            if(GameManager.Instance.CashAmount < _upgradeStackPrice)
                return;
            if(_upgradeStackLevel == 5)
                return;
            if (UIManager.Instance.ItsATutor)
                PassTutorial();
            
            GameManager.Instance.CashAmount -= _upgradeStackPrice;
            _upgradeStackPrice *= DataStorage.Instance.UpgradesSetting.PriceMultipler;
            _stackUpgradePriceText.text = _upgradeStackPrice.ToString();
            _upgradeStackLevel++;
            PlayerPrefs.SetInt(PlayerPrefsKeys.StackPrice, _upgradeStackPrice);
            PlayerPrefs.SetInt(PlayerPrefsKeys.StackLevel, _upgradeStackLevel);
            OnStackUpgradeEvent?.Invoke();
            UpdateButtonStatus();
            _stackFromText.text = GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.MaxStack).ToString();
            _stacktToText.text = (GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.MaxStack) + DataStorage.Instance.UpgradesSetting.StackIncreasePerLevel).ToString();
            for (int i = 0; i < _upgradeStackLevel; i++)
            {
                _stackLevelImages[i].SetActive(true);
            }
            if (_upgradeStackLevel == 5)
            {
                _stackFromText.transform.parent.gameObject.SetActive(false);
                _maxStackLevel.SetActive(true);
                _stackUpgradeButtonActive.SetActive(false);
                //_stackUpgradeButtonUnactive.SetActive(false);
            }
        }

        private void UpdateButtonStatus()
        {
            if (GameManager.Instance.CashAmount < _upgradeStackPrice)
            {
                //_stackUpgradeButtonActive.SetActive(false);
                _stackUpgradeButtonUnactive.SetActive(true);
            }
            else
            {
                //_stackUpgradeButtonActive.SetActive(true);
                _stackUpgradeButtonUnactive.SetActive(false);
            }
            
            if (GameManager.Instance.CashAmount < _upgradeSpeedPrice)
            {
                //_speedUpgradeButtonActive.SetActive(false);
                _speedUpgradeButtonUnactive.SetActive(true);
            }
            else
            {
                //_speedUpgradeButtonActive.SetActive(true);
                _speedUpgradeButtonUnactive.SetActive(false);
            }
            
            if (GameManager.Instance.CashAmount < _upgradeBallPrice)
            {
                //_ballUpgradeButtonActive.SetActive(false);
                _ballUpgradeButtonUnactive.SetActive(true);
            }
            else
            {
                //_ballUpgradeButtonActive.SetActive(true);
                _ballUpgradeButtonUnactive.SetActive(false);
            }
            
            if (GameManager.Instance.CashAmount < _upgradeBodyPrice)
            {
                //_bodyUpgradeButtonActive.SetActive(false);
                _bodyUpgradeButtonUnactive.SetActive(true);
            }
            else
            {
                //_bodyUpgradeButtonActive.SetActive(true);
                _bodyUpgradeButtonUnactive.SetActive(false);
            }
        }

        private void EventSubscriber()
        {
            GameManager.Instance.BaseController.Shop.OnShopActivateEvent += ActivateUpdateMenu;
            GameManager.Instance.BaseController.Shop.OnShopDeactiveEvent += DeactivateUpdateMenu;
        }

        private void LoadSaves()
        {
            _upgradeSpeedLevel = PlayerPrefs.GetInt(PlayerPrefsKeys.SpeedLevel, 0);
            _upgradeBallLevel = PlayerPrefs.GetInt(PlayerPrefsKeys.DamageLevel, 0);
            _upgradeBodyLevel = PlayerPrefs.GetInt(PlayerPrefsKeys.BodySpeedLevel, 0);
            _upgradeStackLevel = PlayerPrefs.GetInt(PlayerPrefsKeys.StackLevel, 0);
            
            _upgradeSpeedPrice = PlayerPrefs.GetInt(PlayerPrefsKeys.SpeedPrice, DataStorage.Instance.UpgradesSetting.StartSpeedUpgradePrice);
            _upgradeBallPrice = PlayerPrefs.GetInt(PlayerPrefsKeys.DamagePrice, DataStorage.Instance.UpgradesSetting.StartDamageUpgradePrice);
            _upgradeBodyPrice = PlayerPrefs.GetInt(PlayerPrefsKeys.BodySpeedPrice, DataStorage.Instance.UpgradesSetting.StartBodyRotateTimeUpgradePrice);
            _upgradeStackPrice = PlayerPrefs.GetInt(PlayerPrefsKeys.StackPrice, DataStorage.Instance.UpgradesSetting.StartMaxStackUpgradePrice);
        }

        private void ActivateUpdateMenu()
        {
            Debug.Log("WTF");
            if (UIManager.Instance.ItsATutor)
            {
                Debug.Log("kek1");
                if (GameManager.Instance.CashAmount < _upgradeBallPrice)
                {
                    Debug.Log("kek2");
                    return;
                }
            }
            UpdateButtonStatus();
            transform.DOMoveY(407.5f, 1f);
            UIManager.Instance.VariableJoystick.gameObject.SetActive(false);
            OnStopMovementEvent?.Invoke();
        }
        
        public void DeactivateUpdateMenu()
        {
            if (!UIManager.Instance.ItsATutor)
            {
                transform.DOMoveY(-465f, 1f);
                UIManager.Instance.VariableJoystick.gameObject.SetActive(true);
                OnStartMovementEvent?.Invoke();
            }
        }

        private void PassTutorial()
        {
            UIManager.Instance.UpgradesDone++;
            if (UIManager.Instance.UpgradesDone >= 1)
            {
                OnTutorDoneEvent?.Invoke();
            }
            else
            {
                UIManager.Instance.UpgradeFinger1.SetActive(false);
                //UIManager.Instance.UpgradeFinger2.SetActive(true);
            }
        }
    }
}