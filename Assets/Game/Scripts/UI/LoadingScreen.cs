﻿using System;
using System.Collections;
using Game.Scripts.Stucts;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] private Image fillSlider;
        private int sceneToLoad;

        private void Awake()
        {
            sceneToLoad = PlayerPrefs.GetInt(PlayerPrefsKeys.CurrentSceneNumber, 1);
        }

        private void Start()
        {
            StartCoroutine(AsyncLoad());
        }

        private IEnumerator AsyncLoad()
        {
            AsyncOperation loadingOperation = SceneManager.LoadSceneAsync(sceneToLoad);
            while (!loadingOperation.isDone)
            {
                fillSlider.fillAmount = loadingOperation.progress + 0.05f;
                yield return null;
            }
        }
    }
}