﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Game.Scripts.UI
{
    public class ProgressBarStar : MonoBehaviour
    {
        private RectTransform _rectTransform;
        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
        }

        private void OnEnable()
        {
            Sequence popUpSequence = DOTween.Sequence();
            popUpSequence.Append(_rectTransform.DOScale(1.5f, 0.5f));
            popUpSequence.Append(_rectTransform.DOScale(1f, 0.2f));
        }

        private void OnDisable()
        {
            _rectTransform.localScale = Vector3.zero;
        }
    }
}