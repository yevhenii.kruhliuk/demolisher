﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Game.Scripts.Core;
using Game.Scripts.TriggerZones;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance { get; private set; }
        public VariableJoystick VariableJoystick { get; private set; }
        public InGameMenu InGameMenu { get; private set; }
        public UpgradeMenu UpgradeMenu { get; private set; }

        public bool ItsATutor;
        public int UpgradesDone;

        public GameObject UpgradeFinger1;
        //public GameObject UpgradeFinger2;

        private void Awake()
        {
            if (!Instance)
                Instance = this;
            else
                Destroy(gameObject);    
            VariableJoystick = GetComponentInChildren<VariableJoystick>();
            InGameMenu = GetComponentInChildren<InGameMenu>();
            UpgradeMenu = GetComponentInChildren<UpgradeMenu>();
            ItsATutor = false;
        }
    }
}