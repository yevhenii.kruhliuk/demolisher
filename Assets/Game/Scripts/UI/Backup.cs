﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Game.Scripts.Core;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class Backup : MonoBehaviour
    {
        public event Action OnStartMovementEvent;
        public event Action OnStopMovementEvent;
        

        [BoxGroup("UpgradeMenu")] [SerializeField] private GameObject _upgradeMenu;
        [BoxGroup("UpgradeMenu")] [SerializeField] private GameObject _attackUpgradesViev;
        [BoxGroup("UpgradeMenu")] [SerializeField] private GameObject _defenceUpgradesViev;
        [BoxGroup("UpgradeMenu")] [SerializeField] private GameObject _carUpgradesViev;
        private List<GameObject> allUpgradeVievs;

        [BoxGroup("UpgradeMenuButtons")] [SerializeField] private GameObject _attackButtonActive;
        [BoxGroup("UpgradeMenuButtons")] [SerializeField] private GameObject _attackButtonUnactive;
        [BoxGroup("UpgradeMenuButtons")] [SerializeField] private GameObject _defenceButtonActive;
        [BoxGroup("UpgradeMenuButtons")] [SerializeField] private GameObject _defenceButtonUnactive;
        [BoxGroup("UpgradeMenuButtons")] [SerializeField] private GameObject _carButtonActive;
        [BoxGroup("UpgradeMenuButtons")] [SerializeField] private GameObject _carButtonUnactive;
        private List<GameObject> allUpgradesMenuButtons;

        [BoxGroup("GunUpgrade")] [SerializeField] private TextMeshProUGUI _gunUpgradePriceText;
        [BoxGroup("GunUpgrade")] [SerializeField] private TextMeshProUGUI _gunFromText;
        [BoxGroup("GunUpgrade")] [SerializeField] private TextMeshProUGUI _gutToText;
        [BoxGroup("GunUpgrade")] [SerializeField] private GameObject[] _gunLevelImages;
        [BoxGroup("GunUpgrade")] [SerializeField] private GameObject _maxGunLevel;
        
        [BoxGroup("CrusherUpgrade")] [SerializeField] private TextMeshProUGUI _crusherUpgradePriceText;
        [BoxGroup("CrusherUpgrade")] [SerializeField] private TextMeshProUGUI _crusherFromText;
        [BoxGroup("CrusherUpgrade")] [SerializeField] private TextMeshProUGUI _crusherToText;
        [BoxGroup("CrusherUpgrade")] [SerializeField] private GameObject[] _crusherLevelImages;
        [BoxGroup("CrusherUpgrade")] [SerializeField] private GameObject _maxCrusherLevel;
        
        [BoxGroup("ThornUpgrade")] [SerializeField] private TextMeshProUGUI _thornUpgradePriceText;
        [BoxGroup("ThornUpgrade")] [SerializeField] private TextMeshProUGUI _thornFromText;
        [BoxGroup("ThornUpgrade")] [SerializeField] private TextMeshProUGUI _thorntToText;
        [BoxGroup("ThornUpgrade")] [SerializeField] private GameObject[] _thornLevelImages;
        [BoxGroup("ThornUpgrade")] [SerializeField] private GameObject _maxThornLevel;
        
        [BoxGroup("SpeedUpgrade")] [SerializeField] private TextMeshProUGUI _speedUpgradePriceText;
        [BoxGroup("SpeedUpgrade")] [SerializeField] private TextMeshProUGUI _speedFromText;
        [BoxGroup("SpeedUpgrade")] [SerializeField] private TextMeshProUGUI _speedtToText;
        [BoxGroup("SpeedUpgrade")] [SerializeField] private GameObject[] _speedLevelImages;
        [BoxGroup("SpeedUpgrade")] [SerializeField] private GameObject _maxSpeedLevel;
        
        [BoxGroup("StackUpgrade")] [SerializeField] private TextMeshProUGUI _stackUpgradePriceText;
        [BoxGroup("StackUpgrade")] [SerializeField] private TextMeshProUGUI _stackFromText;
        [BoxGroup("StackUpgrade")] [SerializeField] private TextMeshProUGUI _stacktToText;
        [BoxGroup("StackUpgrade")] [SerializeField] private GameObject[] _stackLevelImages;
        [BoxGroup("StackUpgrade")] [SerializeField] private GameObject _maxStackLevel;

        private void Awake()
        {

        }

        private void Start()
        {
            EventSubscriber();
            InitializeUpgradesMenu();
        }

        private void EventSubscriber()
        {

        }

        private void InitializeUpgradesMenu()
        {
            CreateUpgradesVievAndButtonsLists();
        }

        /*private void GunViewInitialize()
        {
            _upgradeGunPrice = PlayerPrefs.GetInt(PlayerPrefsKeys.GunUpgradePrice, _gameManager.GunStartPrice);
            _gunUpgradePriceText.text = _upgradeGunPrice.ToString();
            _gunFromText.text = _gameManager.PlayerTruck._gunDamage.ToString();
            _gutToText.text = (_gameManager.PlayerTruck._gunDamage + _gameManager.GunUpgradeStepPerUpgrade).ToString();
            
            for (int i = 0; i < _upgradeGunLevel; i++)
            {
                _gunLevelImages[i].SetActive(true);
            }
            if (_upgradeGunLevel == 5)
            {
                _gunFromText.transform.parent.gameObject.SetActive(false);
                _maxGunLevel.SetActive(true);
                _gunUpgradePriceText.transform.parent.gameObject.SetActive(false);
            }
        }*/

        private void CreateUpgradesVievAndButtonsLists()
        {
            allUpgradeVievs = new List<GameObject>();
            allUpgradeVievs.Add(_attackUpgradesViev);
            allUpgradeVievs.Add(_defenceUpgradesViev);
            allUpgradeVievs.Add(_carUpgradesViev);

            allUpgradesMenuButtons = new List<GameObject>();
            allUpgradesMenuButtons.Add(_attackButtonActive);
            allUpgradesMenuButtons.Add(_attackButtonUnactive);
            allUpgradesMenuButtons.Add(_defenceButtonActive);
            allUpgradesMenuButtons.Add(_defenceButtonUnactive);
            allUpgradesMenuButtons.Add(_carButtonActive);
            allUpgradesMenuButtons.Add(_carButtonUnactive);
        }
        
        

        /*public void NextIsland()
        {
            _gameManager.ResetIsland();
            _gameManager.BaseFrontWall.SetActive(true);
            _progressBarFillImage.fillAmount = 0f;
            _gameManager.CurrentIslandNumber += 1;
            _gameManager.CurrentIsland = _gameManager._islands[_gameManager.CurrentIslandNumber];
            _levelNumText.text = "Level " + (GameManager.Instance.CurrentIslandNumber + 1);
            _gameManager.NextIslandCamera.SetActive(true);
            _gameManager.NextLevelTriggerZone.gameObject.SetActive(false);
            _nextLevelButton.transform.DOMoveY(-100f, 0.5f);
            for (int i = 0; i <= _gameManager.IslandsAmount-1; i++)
            {
                if (i == _gameManager.CurrentIslandNumber)
                    _gameManager.Islands[i].MoveIsland(true);
                else
                    _gameManager.Islands[i].MoveIsland(false);
            }
        }*/

        /*
        private void ChangeCashAmount(int cash)
        {
            _cashAmountText.text = cash.ToString();
        }
        */

        /*public void UpgradeGun()
        {
            if(_gameManager.CashAmount < _upgradeGunPrice)
                return;
            if(_upgradeGunLevel == 5)
                return;
            if (ItsATutor)
            {
                UpgradesDone++;
                if (UpgradesDone >= 2)
                {
                    OnTutorDoneEvent?.Invoke();
                }
                else
                {
                    UpgradeFinger.SetActive(false);
                    UpgradeFinger2.SetActive(true);
                }
            }
            _gameManager.CashAmount -= _upgradeGunPrice;
            _upgradeGunPrice *= 2;
            _gunUpgradePriceText.text = _upgradeGunPrice.ToString();
            _upgradeGunLevel++;
            OnGunUpgrade?.Invoke(_upgradeGunLevel);
            _gameManager.PlayerTruck._gunDamage += _gameManager.GunUpgradeStepPerUpgrade;
            _gunFromText.text = _gameManager.PlayerTruck._gunDamage.ToString();
            _gutToText.text = (_gameManager.PlayerTruck._gunDamage + _gameManager.GunUpgradeStepPerUpgrade).ToString();
            for (int i = 0; i < _upgradeGunLevel; i++)
            {
                _gunLevelImages[i].SetActive(true);
            }
            if (_upgradeGunLevel == 5)
            {
                _gunFromText.transform.parent.gameObject.SetActive(false);
                _maxGunLevel.SetActive(true);
                _gunUpgradePriceText.transform.parent.gameObject.SetActive(false);
            }
        }*/

        /*public void VibrationStatusSwitcher(bool isActive)
        {
            if (!isActive)
            {
                _vibrationButtonOff.SetActive(false);
                _vibrationButtonOn.SetActive(true);
            }
            else
            {
                _vibrationButtonOff.SetActive(true);
                _vibrationButtonOn.SetActive(false);
            }
            GameManager.Instance.IsVibrationActive = isActive;
        }

        public void AudioStatusSwitcher(bool isActive)
        {
            if (!isActive)
            {
                _audioButtonOff.SetActive(false);
                _audioButtonOn.SetActive(true);
            }
            else
            {
                _audioButtonOff.SetActive(true);
                _audioButtonOn.SetActive(false);
            }
            GameManager.Instance.IsAudioActive = isActive;
        }

        private void ActivateUpdateMenu()
        {
            _upgradeMenu.transform.DOMoveY(407.5f, 1f);
            _variableJoystick.gameObject.SetActive(false);
            OnStopMovementEvent?.Invoke();
        }
        
        public void DeactivateUpdateMenu()
        {
            _upgradeMenu.transform.DOMoveY(-420f, 1f);
            _variableJoystick.gameObject.SetActive(true);
            OnStartMovementEvent?.Invoke();
        }

        private void ActivateNextLevelButton()
        {
            _nextLevelButton.transform.DOMoveY(300f, 0.5f);
        }

        private void DeactivateNextLevelButton()
        {
            _nextLevelButton.transform.DOMoveY(-100f, 0.5f);
        }

        public void ActivateMaxStackText(bool isActive)
        {
            if (isActive)
                _maxStackText.transform.DOScale(1f, 1f);
            else
                _maxStackText.transform.DOScale(0f, 0.5f);
        }*/

        public void ChangeUpgradeMenu(string menuName)
        {
            foreach (var viev in allUpgradeVievs)
            {
                viev.SetActive(false);
            }
            foreach (var button in allUpgradesMenuButtons)
            {
                button.SetActive(false);
            }

            switch (menuName)
            {
                case "ATTACK":
                    _attackUpgradesViev.SetActive(true);
                    _attackButtonActive.SetActive(true);
                    _defenceButtonUnactive.SetActive(true);
                    _carButtonUnactive.SetActive(true);
                    break;
                case "DEFENCE":
                    _defenceUpgradesViev.SetActive(true);
                    _defenceButtonActive.SetActive(true);
                    _attackButtonUnactive.SetActive(true);
                    _carButtonUnactive.SetActive(true);
                    break;
                case "CAR":
                    _carUpgradesViev.SetActive(true);
                    _carButtonActive.SetActive(true);
                    _attackButtonUnactive.SetActive(true);
                    _defenceButtonUnactive.SetActive(true);
                    break;
            }
        }

        public void SaveUIData()
        {

        }
    }
}