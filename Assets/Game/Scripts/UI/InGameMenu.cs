﻿using System;
using System.Collections;
using System.Collections.Generic;
using AppsFlyerSDK;
using DG.Tweening;
using Game.Scripts.Core;
using Game.Scripts.Stucts;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class InGameMenu : MonoBehaviour
    {
        public event Action OnCongratsFinishEvent;
        public event Action OnNextSceneActivate;
        public event Action OnNextIslandEvent;
        public int BuildPartsCount { get; set; }
        public int BrokenBuildPartsCount
        {
            get => _brokernPartsCount;
            set
            {
                _brokernPartsCount = value;
                float currentEnemyPct = (float) _brokernPartsCount / (float) BuildPartsCount;
                ChangeProgressBar(currentEnemyPct);
            }
        }
        
        private int _brokernPartsCount;
        [BoxGroup("InGameScreen")] [SerializeField] private GameObject _vibrationButtonOff;
        [BoxGroup("InGameScreen")] [SerializeField] private GameObject _vibrationButtonOn;
        [BoxGroup("InGameScreen")] [SerializeField] private GameObject _audioButtonOff;
        [BoxGroup("InGameScreen")] [SerializeField] private GameObject _audioButtonOn;
        [BoxGroup("InGameScreen")] [SerializeField] private TextMeshProUGUI _cashAmountText;
        [BoxGroup("InGameScreen")] [SerializeField] private Image _progressBarFillImage;
        [BoxGroup("InGameScreen")] [SerializeField] private GameObject _congratulationScreen;
        [BoxGroup("InGameScreen")] [SerializeField] private GameObject _nextLevelButton;
        [BoxGroup("InGameScreen")] [SerializeField] private GameObject _maxStackText;
        [BoxGroup("InGameScreen")] [SerializeField] private TextMeshProUGUI _levelNumText;

        //[BoxGroup("Stars")] [SerializeField] private GameObject _unactiveFirstStar;
        [BoxGroup("Stars")] [SerializeField] private GameObject _activeFirstStar;
        //[BoxGroup("Stars")] [SerializeField] private GameObject _unactiveSecondStar;
        [BoxGroup("Stars")] [SerializeField] private GameObject _activeSecondStar;
        //[BoxGroup("Stars")] [SerializeField] private GameObject _unactiveThirdStar;
        [BoxGroup("Stars")] [SerializeField] private GameObject _activeThirdStar;

        public int _levelCounter;
        private bool _isWinScreenActivate;

        private void Awake()
        {
            _levelCounter = PlayerPrefs.GetInt(PlayerPrefsKeys.LevelCounter, 1);
        }

        private void Start()
        {
            _isWinScreenActivate = false;
            EventSubscriber();
            _levelNumText.text = "Level " + _levelCounter;
        }

        private void EventSubscriber()
        {
            GameManager.Instance.OnCashAmountChangedEvent += ChangeCashAmountText;
            GameManager.Instance.BaseController.NextLevelTriggerZone.OnNextLevelZoneActivateEvent += ActivateNextLevelButton;
            GameManager.Instance.BaseController.NextLevelTriggerZone.OnNextLevelZoneDeactivateEvent += DeactivateNextLevelButton;
        }

        private void ChangeCashAmountText(int amount)
        {
            _cashAmountText.text = amount.ToString();
        }

        private void ChangeProgressBar(float pct)
        {
            StartCoroutine(ChangingProgressBar(pct));
        }

        private IEnumerator ChangingProgressBar(float pct)
        {
            float preChangePct = _progressBarFillImage.fillAmount;
            float elapsed = 0f;
            while (elapsed < 0.2f)
            {
                elapsed += Time.deltaTime;
                _progressBarFillImage.fillAmount = Mathf.Lerp(preChangePct, pct, elapsed / 0.2f);
                yield return null;
            }
            _progressBarFillImage.fillAmount = pct;
            if (_progressBarFillImage.fillAmount >= GameManager.Instance.Islands[GameManager.Instance.CurrentIslandNumber].PercentToComplete)
            {
                _activeFirstStar.SetActive(true);
                if (!_isWinScreenActivate)
                {
                    ActivateCongratulations();
                    _isWinScreenActivate = true;
                }
            }

            if (_progressBarFillImage.fillAmount >= 0.75f)
            {
                _activeSecondStar.SetActive(true);
            }
            if (_progressBarFillImage.fillAmount >= 0.9f)
            {
                _activeThirdStar.SetActive(true);
            }
        }

        private void ActivateCongratulations()
        {
            Dictionary<string, string> eventValues = new Dictionary<string, string>();
            eventValues.Add("LEVEL_NUMBER", UIManager.Instance.InGameMenu._levelCounter.ToString());
            AppsFlyer.sendEvent("LEVEL_COMPLETE", eventValues);
            _congratulationScreen.SetActive(true);
            StartCoroutine(CongratsFinishDelay());
            GameManager.Instance.BaseController.NextLevelTriggerZone.gameObject.SetActive(true);
        }

        private IEnumerator CongratsFinishDelay()
        {
            yield return new WaitForSecondsRealtime(2f);
            OnCongratsFinishEvent?.Invoke();
        }
        
        public void NextIsland()
        {
            BuildPartsCount = 0;
            BrokenBuildPartsCount = 0;
            _brokernPartsCount = 0;
            GameManager.Instance.ClearUnReceiveItems();
            GameManager.Instance.BaseController.BaseFrontWall.SetActive(true);
            _progressBarFillImage.fillAmount = 0f;
            GameManager.Instance.CurrentIslandNumber += 1;
            _levelCounter += 1;
            _levelNumText.text = "Level " + _levelCounter;
            PlayerPrefs.SetInt(PlayerPrefsKeys.LevelCounter, _levelCounter);
            GameManager.Instance.BaseController.NextLevelTriggerZone.gameObject.SetActive(false);
            _nextLevelButton.transform.DOMoveY(-100f, 0.5f);
            OnNextIslandEvent?.Invoke();
            _activeFirstStar.SetActive(false);
            _activeSecondStar.SetActive(false);
            _activeThirdStar.SetActive(false);
            GameManager.Instance.SaveData();
            if (GameManager.Instance.CurrentIslandNumber <= GameManager.Instance.Islands.Count - 1)
            {
                MoveIslands();
            }
            else
            {
                LoadNextScene();
            }
        }

        private void MoveIslands()
        {
            Dictionary<string, string> eventValues = new Dictionary<string, string>();
            eventValues.Add("LEVEL_NUMBER", UIManager.Instance.InGameMenu._levelCounter.ToString());
            AppsFlyer.sendEvent("LEVEL_STARTED", eventValues);
            for (int i = 0; i <= GameManager.Instance.Islands.Count - 1; i++)
            {
                if (i == GameManager.Instance.CurrentIslandNumber)
                    GameManager.Instance.Islands[i].MoveIsland(true);
                else
                {
                    GameManager.Instance.Islands[i].MoveIsland(false);
                }
            }
            _isWinScreenActivate = false;
            
        }

        private void LoadNextScene()
        {
            _isWinScreenActivate = false;
            GameManager.Instance.CurrentIslandNumber = 0;
            OnNextSceneActivate?.Invoke();
            GameManager.Instance.SaveData();
            int currentScene = PlayerPrefs.GetInt(PlayerPrefsKeys.CurrentSceneNumber, 1);
            if (currentScene == 1)
            {
                PlayerPrefs.SetInt(PlayerPrefsKeys.CurrentSceneNumber, 2);
                SceneManager.LoadScene(4);
            }
            else if (currentScene == 2)
            {
                PlayerPrefs.SetInt(PlayerPrefsKeys.CurrentSceneNumber, 3);
                SceneManager.LoadScene(4);
            }
            else if (currentScene == 3)
            {
                PlayerPrefs.SetInt(PlayerPrefsKeys.CurrentSceneNumber, 1);
                SceneManager.LoadScene(4);
            }
        }
        
        public void MaxStackTextSwitcher(bool isActive)
        {
            if (isActive)
                _maxStackText.transform.DOScale(1f, 1f);
            else
                _maxStackText.transform.DOScale(0f, 0.5f);
        }
        
        public void VibrationStatusSwitcher(bool isActive)
        {
            if (!isActive)
            {
                _vibrationButtonOff.SetActive(false);
                _vibrationButtonOn.SetActive(true);
            }
            else
            {
                _vibrationButtonOff.SetActive(true);
                _vibrationButtonOn.SetActive(false);
            }
            GameManager.Instance.IsVibrationActive = isActive;
        }
        
        public void AudioStatusSwitcher(bool isActive)
        {
            if (!isActive)
            {
                _audioButtonOff.SetActive(false);
                _audioButtonOn.SetActive(true);
            }
            else
            {
                _audioButtonOff.SetActive(true);
                _audioButtonOn.SetActive(false);
            }
            GameManager.Instance.IsAudioActive = isActive;
        }
        
        private void ActivateNextLevelButton()
        {
            _nextLevelButton.transform.DOMoveY(300f, 0.5f);
        }

        private void DeactivateNextLevelButton()
        {
            _nextLevelButton.transform.DOMoveY(-100f, 0.5f);
        }
    }
}