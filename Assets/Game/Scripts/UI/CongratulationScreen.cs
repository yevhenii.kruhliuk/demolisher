﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.UI
{
    public class CongratulationScreen : MonoBehaviour
    {
        [SerializeField] private Image CompleteText;
        [SerializeField] private Animator[] Confetties;

        private void Start()
        {
            UIManager.Instance.InGameMenu.OnCongratsFinishEvent += OffCongratulationScreen;
        }

        private void OnEnable()
        {
            CompleteText.transform.DOScale(1f, 1f);
            foreach (var vfx in Confetties)
            {
                vfx.SetTrigger("Play");
            }
        }

        private void OffCongratulationScreen()
        {
            CompleteText.transform.DOScale(0f, 0.5f).OnComplete(()=>gameObject.SetActive(false));
        }
    }
}