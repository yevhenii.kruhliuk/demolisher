﻿namespace Game.Scripts.Stucts
{
    public struct Tags
    {
        public static string Player = "Player";
        public static string Factory = "Factory";
        public static string Item = "Item";
        public static string Ball = "Ball";
    }
}