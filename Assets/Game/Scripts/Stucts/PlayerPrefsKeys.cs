﻿namespace Game.Scripts.Stucts
{
    public struct PlayerPrefsKeys
    {
        public static string CashAmount = "CashAmount";
        public static string CurrentSceneNumber = "CurrentSceneNumber";
        public static string CurrentIslandNumber = "CurrentIslandNumber";
        public static string LevelCounter = "LevelCounter";
        public static string IsTutorialDone = "IsTutorialDone";
        public static string TutorialFirstStep = "TutorialFirstStep";
        public static string TutorialSecondStep = "TutorialSecondStep";
        public static string TutorialThirdStep = "TutorialThirdStep";
        public static string CanUseShop = "CanUseShop";

        public static string Speed = "Speed";
        public static string Damage = "Damage";
        public static string MaxStack = "MaxStack";
        public static string BodySpeed = "BodySpeed";
        
        public static string SpeedLevel = "SpeedLevel";
        public static string DamageLevel = "DamageLevel";
        public static string StackLevel = "StackLevel";
        public static string BodySpeedLevel = "BodySpeedLevel";
        
        public static string SpeedPrice = "SpeedPrice";
        public static string DamagePrice = "DamagePrice";
        public static string StackPrice = "MaxStackPrice";
        public static string BodySpeedPrice = "BodySpeedPrice";
    }
}