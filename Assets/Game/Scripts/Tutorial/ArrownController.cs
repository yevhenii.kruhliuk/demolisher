﻿using System;
using Game.Scripts.Core;
using Game.Scripts.Demolisher;
using Game.Scripts.Stucts;
using Game.Scripts.TriggerZones;
using Game.Scripts.UI;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Tutorial
{
    public class ArrownController : MonoBehaviour
    {
        public event Action OnShopTutorialEvent;
        
        [BoxGroup("Event Holders")][SerializeField] private NavigateArrow _navigateArrow;
        [BoxGroup("Event Holders")][SerializeField] private MagneticField _magneticField;
        [BoxGroup("Event Holders")][SerializeField] private StackController _stackController;
        
        [BoxGroup("Navigation Points")][SerializeField] private GameObject _bridge;
        [BoxGroup("Navigation Points")][SerializeField] private GameObject _factory;
        [BoxGroup("Navigation Points")][SerializeField] private GameObject _shop;
        [BoxGroup("Navigation Points")][SerializeField] private GameObject _nextLevelZone;
        
        [BoxGroup("Arrows")][SerializeField] private GameObject _factoryArrow;
        [BoxGroup("Arrows")][SerializeField] private GameObject _shopArrow;
        [BoxGroup("Arrows")][SerializeField] private GameObject _nextLevelZoneArrow;

        private bool _toFactory;
        private bool _toNextLevel;
        private void Start()
        {
            _magneticField.OnInFactoryZoneEvent += FinishNavigateToFactory;
            _stackController.OnStackFullEvent += NavigateToFactory;
            UIManager.Instance.InGameMenu.OnCongratsFinishEvent += NavigateToNextLevel;
            UIManager.Instance.InGameMenu.OnNextIslandEvent += FinishNavigateToNextLevel;
            GameManager.Instance.BaseController.BaseTriggerZone.OnComeToBaseEvent += ChangeWay;
        }
        
        private void NavigateToFactory()
        {
            _navigateArrow.gameObject.SetActive(true);
            _navigateArrow.smth = _bridge.transform;
            _factoryArrow.SetActive(true);
            _toFactory = true;
        }

        private void FinishNavigateToFactory()
        {
            _navigateArrow.gameObject.SetActive(false);
            _factoryArrow.SetActive(false);
            if (PlayerPrefs.GetInt(PlayerPrefsKeys.TutorialSecondStep) == 0)
            {
                OnShopTutorialEvent?.Invoke();
            }
        }
        
        private void NavigateToNextLevel()
        {
            _navigateArrow.gameObject.SetActive(true);
            _navigateArrow.smth = _bridge.transform;
            _nextLevelZoneArrow.SetActive(true);
            _toNextLevel = true;
        }

        private void FinishNavigateToNextLevel()
        {
            _navigateArrow.gameObject.SetActive(false);
            _nextLevelZoneArrow.SetActive(false);
        }

        private void ChangeWay()
        {
            if (_toFactory)
                _navigateArrow.smth = _factory.transform;
            else if (_toNextLevel)
                _navigateArrow.smth = _nextLevelZone.transform;    
        }
    }
}