﻿using System;
using Game.Scripts.Core;
using Game.Scripts.Demolisher;
using Game.Scripts.Stucts;
using Game.Scripts.TriggerZones;
using Game.Scripts.UI;
using UnityEngine;

namespace Game.Scripts.Tutorial
{
    public class Tutorial : MonoBehaviour
    {
        public static Tutorial singleton;

        [SerializeField] private ArrownController _arrownController;
        [SerializeField] private Ball _ball;
        [SerializeField] private Shop _shop;
        [SerializeField] private GameObject _firstBuild;
        
        [SerializeField] private NavigateArrow _navigateArrow;
        [SerializeField] private GameObject _firstEnemyArrow;
        [SerializeField] private GameObject _killBlobsText;

        [SerializeField] private GameObject _shopArrow;
        

        public int _firstStepDone;
        public int _secondStepDone;
        public int _thirdStepDone;

        private void Awake()
        {
            int isTutorialDone = PlayerPrefs.GetInt(PlayerPrefsKeys.IsTutorialDone, 0);
            PlayerPrefs.SetInt(PlayerPrefsKeys.IsTutorialDone, isTutorialDone);
            if (isTutorialDone == 0)
            {
                if (!singleton)
                    singleton = this;
                else
                    Destroy(gameObject);
            }
            else
            {
                LoadSaves();
                FinishAll();
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            _ball.OnHitEvent += FinishFirstStep;
            _arrownController.OnShopTutorialEvent += StartSecondStep;
            _shop.OnFirstTimeUpgrade += FinishSecondStep;
            UIManager.Instance.UpgradeMenu.OnTutorDoneEvent += FinishThirdStep;
            LoadSaves();
            StartFirstStep();
        }

        private void FinishAll()
        {
            FinishFirstStep();
            FinishSecondStep();
            FinishThirdStep();
        }

        private void LoadSaves()
        {
            _firstStepDone = PlayerPrefs.GetInt(PlayerPrefsKeys.TutorialFirstStep, 0);
            if(_firstStepDone == 1)
                FinishFirstStep();
            _secondStepDone = PlayerPrefs.GetInt(PlayerPrefsKeys.TutorialSecondStep, 0);
            if (_secondStepDone == 1)
                FinishSecondStep();
            _thirdStepDone = PlayerPrefs.GetInt(PlayerPrefsKeys.TutorialThirdStep, 0);
            if (_thirdStepDone == 1)
                FinishThirdStep();
        }

        private void StartFirstStep()
        {
            _killBlobsText.SetActive(true);
            _navigateArrow.gameObject.SetActive(true);
            _navigateArrow.smth = _firstBuild.transform;
            _firstEnemyArrow.SetActive(true);
        }

        private void FinishFirstStep()
        {
            if (_firstStepDone == 0)
            {
                _navigateArrow.gameObject.SetActive(false);
                _killBlobsText.SetActive(false);
                _firstStepDone = 1;
                PlayerPrefs.SetInt("TutorialFirstStep", 1);
            }
            if(_firstEnemyArrow)
                Destroy(_firstEnemyArrow);
        }

        private void StartSecondStep()
        {
            _navigateArrow.gameObject.SetActive(true);
            _navigateArrow.smth = _shop.transform;
            _shopArrow.SetActive(true);
            PlayerPrefs.SetInt(PlayerPrefsKeys.CanUseShop, 1);
            GameManager.Instance.CanUseShop = true;
        }

        private void FinishSecondStep()
        {
            UIManager.Instance.ItsATutor = true;
            _navigateArrow.gameObject.SetActive(false);
            _shopArrow.SetActive(false);
            _secondStepDone = 1;
            PlayerPrefs.SetInt(PlayerPrefsKeys.TutorialSecondStep, 1);
            if (_thirdStepDone == 0)
            {
                StartThirdStep();
            }
        }

        private void StartThirdStep()
        {
            UIManager.Instance.UpgradeFinger1.SetActive(true);
        }

        private void FinishThirdStep()
        {
            _thirdStepDone = 1;
            PlayerPrefs.SetInt(PlayerPrefsKeys.TutorialThirdStep, 1);
            UIManager.Instance.ItsATutor = false;
            UIManager.Instance.UpgradeFinger1.SetActive(false);
            //UIManager.Instance.UpgradeFinger2.SetActive(false);
            PlayerPrefs.SetInt(PlayerPrefsKeys.IsTutorialDone, 1);
        }
    }
}