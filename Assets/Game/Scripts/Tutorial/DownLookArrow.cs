﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Game.Scripts.Tutorial
{
    public class DownLookArrow : MonoBehaviour
    {
        [SerializeField] private Vector3 _startPos;
        [SerializeField] private float _time = 0f;
        [SerializeField] private float _offset = 0f;
        [SerializeField] private float _amp = 0.5f;
        [SerializeField] private float _freq = 5f;
        private void Start()
        {
            _startPos = transform.position;
        }

        private void Update()
        {
            _time += Time.deltaTime;
            _offset = _amp * Mathf.Sin(_time * _freq);
            transform.position = _startPos + new Vector3(0,_offset,0);
        }
    }
}