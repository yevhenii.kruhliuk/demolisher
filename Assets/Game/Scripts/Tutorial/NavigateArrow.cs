﻿using UnityEngine;

namespace Game.Scripts.Tutorial
{
    public class NavigateArrow : MonoBehaviour
    {
        public Transform smth;

        private void Update()
        {
            if (smth != null)
            {
                transform.LookAt(smth);
            }
        }
    }
}