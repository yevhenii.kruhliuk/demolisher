﻿using System;
using UnityEngine;

namespace Game.Scripts.Demolisher
{
    public class LineRendererController : MonoBehaviour
    {
        private LineRenderer _lineRenderer;

        private void Awake()
        {
            _lineRenderer = GetComponent<LineRenderer>();
            _lineRenderer.positionCount = 2;
        }

        private void LateUpdate()
        {
            _lineRenderer.SetPosition(0,transform.position);
            _lineRenderer.SetPosition(1,transform.GetChild(0).position);
        }
    }
}