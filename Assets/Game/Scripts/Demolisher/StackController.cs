﻿using System;
using System.Collections.Generic;
using Game.Scripts.Core;
using Game.Scripts.UI;
using Gameplay.Stack;
using UnityEngine;

namespace Game.Scripts.Demolisher
{
    public class StackController : MonoBehaviour
    {
        public event Action OnStackFullEvent;
        
        private List<ItemStack> _itemStacks;
        private Demolisher _demolisher;
        private int _stackSize;

        private void Awake()
        {
            _demolisher = GetComponentInParent<Demolisher>();
            _itemStacks = new List<ItemStack>();
            for (int i = 0; i < transform.childCount; i++)
            {
                _itemStacks.Add(transform.GetChild(i).GetComponent<ItemStack>());
            }
            GameManager.Instance.Demolisher.OnChangeStackSizeEvent += SetStackSize;
        }

        private void Start()
        {
            UIManager.Instance.InGameMenu.OnNextSceneActivate += SellAllItems;
        }

        private void Update()
        {
            CheckIfFull();
        }

        private void CheckIfFull()
        {
            int amount = 0;
            foreach (var stack in _itemStacks)
            {
                amount += stack.Count;
            }

            if (amount == _stackSize && !_demolisher.IsStackFull)
            {
                _demolisher.IsStackFull = true;
                OnStackFullEvent?.Invoke();
                UIManager.Instance.InGameMenu.MaxStackTextSwitcher(true);
            }
            else if (amount < _stackSize && _demolisher.IsStackFull)
            {
                _demolisher.IsStackFull = false;
                UIManager.Instance.InGameMenu.MaxStackTextSwitcher(false);
            }
        }

        private void SetStackSize()
        {
            _stackSize = (int)_demolisher.GetDemolisherData(Demolisher.DemolisherDataType.MaxStack);
            foreach (var stack in _itemStacks)
            {
                stack.SetCapacity(_stackSize / _itemStacks.Count);
            }
        }

        private void SellAllItems()
        {
            int itemsAmount = 0;
            foreach (var stack in _itemStacks)
            {
                itemsAmount += stack.Count;
            }
            itemsAmount *= GameManager.Instance.BaseController.Factory.BlobPrice;
            GameManager.Instance.CashAmount += itemsAmount;
        }
    }
}