﻿using System;
using System.Collections.Generic;
using Game.Scripts.Core;
using Game.Scripts.ScriptableObjects;
using Game.Scripts.Stucts;
using Game.Scripts.UI;
using Obi;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Demolisher
{
    public class Demolisher : MonoBehaviour
    {
        public event Action OnChangeStackSizeEvent;
        public enum DemolisherDataType
        {
            Speed,
            Damage,
            MaxStack,
            BodyRotateTime
        }

        public bool IsStackFull;

        [SerializeField] private float _currentMoveSpeed;
        [SerializeField] private float _currentDamage;
        [SerializeField] private int _currentMaxStack;
        [SerializeField] private float _bodyRotateTime;
        
        [BoxGroup("Speeds")] [SerializeField] private List<GameObject> _speeds;
        [BoxGroup("Balls")] [SerializeField] private List<GameObject> _balls;
        [BoxGroup("Bodies")] [SerializeField] private List<GameObject> _bodies;

        [SerializeField] private ParticleSystem _upgradeVFX;

        private void Awake()
        {
            LoadSaves();
        }

        private void Start()
        {
            EventSubscriber();
            OnChangeStackSizeEvent?.Invoke();
            SetUpgradesAtStart();
        }
        
        private void SetUpgradesAtStart()
        {
            int speedLvl = PlayerPrefs.GetInt(PlayerPrefsKeys.SpeedLevel,0);
            int ballLevel = PlayerPrefs.GetInt(PlayerPrefsKeys.DamageLevel,0);
            int bodyLevel = PlayerPrefs.GetInt(PlayerPrefsKeys.BodySpeedLevel, 0);
            
            _speeds[speedLvl].SetActive(true);
            _bodies[bodyLevel].SetActive(true);
            _balls[ballLevel].SetActive(true);
        }

        private void UpgradeSpeed(int level)
        {
            _currentMoveSpeed += DataStorage.Instance.UpgradesSetting.SpeedIncreasePerLevel;
            PlayerPrefs.SetFloat(PlayerPrefsKeys.Speed, _currentMoveSpeed);
            foreach (var elem in _speeds)
            {
                if(elem!=null)
                    elem.SetActive(false);
            }
            _speeds[level].SetActive(true);
            _upgradeVFX.Play();
        }
        
        private void UpgradeBall(int level)
        {
            _currentDamage *= DataStorage.Instance.UpgradesSetting.DamageIncreasePerLevel;
            PlayerPrefs.SetFloat(PlayerPrefsKeys.Damage, _currentDamage);
            foreach (var elem in _balls)
            {
                if(elem!=null)
                    elem.SetActive(false);
            }
            _balls[level].SetActive(true);
            _upgradeVFX.Play();
        }
        
        private void UpgradeBody(int level)
        {
            _bodyRotateTime -= DataStorage.Instance.UpgradesSetting.BodyIncreasePerLevel;
            PlayerPrefs.SetFloat(PlayerPrefsKeys.BodySpeed, _bodyRotateTime);
            foreach (var elem in _bodies)
            {
                if(elem!=null)
                    elem.SetActive(false);
            }
            _bodies[level].SetActive(true);
            _upgradeVFX.Play();
        }
        
        private void UpgradeStack()
        {
            _currentMaxStack += DataStorage.Instance.UpgradesSetting.StackIncreasePerLevel;
            PlayerPrefs.SetInt(PlayerPrefsKeys.MaxStack, _currentMaxStack);
            OnChangeStackSizeEvent?.Invoke();
            _upgradeVFX.Play();
        }

        public float GetDemolisherData(DemolisherDataType dataType)
        {
            switch (dataType)
            {
                case DemolisherDataType.Speed:
                    return _currentMoveSpeed;
                case DemolisherDataType.Damage:
                    return _currentDamage;
                case DemolisherDataType.MaxStack:
                    return _currentMaxStack;
                case DemolisherDataType.BodyRotateTime:
                    return _bodyRotateTime;
                default:
                    return 0f;
            }
        }

        private void EventSubscriber()
        {
            UIManager.Instance.UpgradeMenu.OnSpeedUpgradeEvent += UpgradeSpeed;
            UIManager.Instance.UpgradeMenu.OnBallUpgradeEvent += UpgradeBall;
            UIManager.Instance.UpgradeMenu.OnBodyUpgradeEvent += UpgradeBody;
            UIManager.Instance.UpgradeMenu.OnStackUpgradeEvent += UpgradeStack;
        }

        private void LoadSaves()
        {
            _currentMoveSpeed = PlayerPrefs.GetFloat(PlayerPrefsKeys.Speed, DataStorage.Instance.DemolisherStartParams.StartSpeed);
            _currentDamage = PlayerPrefs.GetFloat(PlayerPrefsKeys.Damage, DataStorage.Instance.DemolisherStartParams.StartDamage);
            _currentMaxStack = PlayerPrefs.GetInt(PlayerPrefsKeys.MaxStack, DataStorage.Instance.DemolisherStartParams.StartMaxStack);
            _bodyRotateTime = PlayerPrefs.GetFloat(PlayerPrefsKeys.BodySpeed, DataStorage.Instance.DemolisherStartParams.BodyRotateTime);
        }
    }
}