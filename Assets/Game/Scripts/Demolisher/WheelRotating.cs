﻿using System;
using Game.Scripts.Controll;
using UnityEngine;

namespace Game.Scripts.Demolisher
{
    public class WheelRotating : MonoBehaviour
    {
        private Movement _movement;
        private bool _isMoving;
        private float speed;
        private void Start()
        {
            _movement = GetComponentInParent<Movement>();
        }

        private void Update()
        {
            _isMoving = _movement.IsMoving;
            speed = _movement.GetCurrentMovespeed() * 25f;
        }

        private void FixedUpdate()
        {
            if (_isMoving)
            {
                transform.Rotate(speed * Time.deltaTime, 0f,0f, Space.Self);
            }
            else
            {
                transform.Rotate(0f,0f,0f);
            }
        }
    }
}