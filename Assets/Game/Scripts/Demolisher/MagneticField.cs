﻿using System;
using System.Collections.Generic;
using Game.Scripts.ObjectPool;
using Game.Scripts.Stucts;
using Gameplay.Stack.Interactions;
using Gameplay.Stack.Item;
using UnityEngine;
using Utils;

namespace Game.Scripts.Demolisher
{
    public class MagneticField : MonoBehaviour
    {
        public event Action OnInFactoryZoneEvent;
        
        public LayerMask LayerMask;
        
        [SerializeField] private float fieldSize;
        [SerializeField] private Ref<IItemReceiver> _receiver;
        [SerializeField] private Ref<IItemSource> _source;

        private Demolisher _demolisher;
        private readonly Dictionary<Collider, IItemReceiver> _receivers = new Dictionary<Collider, IItemReceiver>();
        private const float Timer = 0.05f;
        private float _timer;
        
        private static readonly RaycastHit[] _hits = new RaycastHit[128];

        private void Start()
        {
            _demolisher = GetComponent<Demolisher>();
        }

        private void Update()
        {
            if(_demolisher.IsStackFull)
                return;
            if(!_receiver.Value.CanReceive)
                return;
            CollectItems();
        }

        private void CollectItems()
        {
            int found = Physics.SphereCastNonAlloc(transform.position, 
                fieldSize, transform.forward, _hits, 10, LayerMask);
            for (int i = 0; i < found; i++)
            {
                RaycastHit hit = _hits[i];
                if (hit.collider.CompareTag(Tags.Item) && hit.collider.TryGetComponent(out SomeItem blobItem))
                {
                    if(!blobItem.ReadyToCollect)
                        continue;
                    var config = blobItem.ItemConfig;
                    var item = ItemPool.Instance.ObjectPool.Get(hit.transform.position, Quaternion.identity);
                    if(!_receiver.Value.CanReceive)
                        return;
                    item.Load(config);
                    _receiver.Value.AddItem(item);
                    hit.transform.gameObject.SetActive(false);
                }
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if(!other.CompareTag(Tags.Factory))
                return;
            OnInFactoryZoneEvent?.Invoke();
        }

        private void OnTriggerStay(Collider other)
        {
            _timer -= Time.fixedDeltaTime;
            if(!other.CompareTag(Tags.Factory))
                return;
            if(_timer>0)
                return;
            if(!_receivers.ContainsKey(other))
                _receivers.Add(other, other.GetComponent<IItemReceiver>());
            if (_source.Value.TryPopItem(out ItemBase item))
            {
                _receivers[other].AddItem(item);
                _timer = Timer;
            }
        }
    }
}