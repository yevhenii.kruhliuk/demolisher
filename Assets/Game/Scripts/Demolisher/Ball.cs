﻿using System;
using DG.Tweening;
using Game.Scripts.Buildings;
using Game.Scripts.Core;
using Game.Scripts.Stucts;
using MoreMountains.NiceVibrations;
using UnityEngine;

namespace Game.Scripts.Demolisher
{
    public class Ball : MonoBehaviour
    {
        public event Action OnHitEvent;
        [SerializeField] private ParticleSystem _hitVFX;
        private void OnCollisionEnter(Collision collision)
        {
            if(collision.gameObject.layer != Layers.Building)
                return;
            if (collision.collider.TryGetComponent(out BuildPart part))
            {
                part.HP -= GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.DemolisherDataType.Damage);
                _hitVFX.Play();
                GameManager.Instance.CameraManager.CameraShake(5f, 0.1f);
                GameManager.Instance.UseVibration(HapticTypes.MediumImpact);
                if (PlayerPrefs.GetInt(PlayerPrefsKeys.TutorialFirstStep) == 0)
                {
                    OnHitEvent?.Invoke();
                }
            }
            else if (collision.collider.TryGetComponent(out SuperBuilding build))
            {
                build.ShakeBuild();
                _hitVFX.Play();
                //build.PlayHitVFX();
                build.HP -= GameManager.Instance.Demolisher.GetDemolisherData(Demolisher.DemolisherDataType.Damage);
                GameManager.Instance.CameraManager.CameraShake(5f, 0.1f);
                GameManager.Instance.UseVibration(HapticTypes.MediumImpact);
                if (PlayerPrefs.GetInt(PlayerPrefsKeys.TutorialFirstStep) == 0)
                {
                    OnHitEvent?.Invoke();
                }
            }
        }
    }
}