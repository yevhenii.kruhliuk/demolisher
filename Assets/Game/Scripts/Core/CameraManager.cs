﻿using System;
using Cinemachine;
using Game.Scripts.TriggerZones;
using UnityEngine;

namespace Game.Scripts.Core
{
    public class CameraManager : MonoBehaviour
    {
        [SerializeField] private BaseTriggerZone _baseTriggerZone;
        [SerializeField] private GameObject _mainPlayerCamera;
        [SerializeField] private GameObject _basePlayerCamera;

        private CinemachineVirtualCamera _mainCamera;
        private CinemachineBasicMultiChannelPerlin _mainCameraPerlin;
        private float _shakeTimer;

        private void Start()
        {
            _baseTriggerZone.OnComeToBaseEvent += ActivateBaseCamera;
            _baseTriggerZone.OnLeaveBaseEvent += DeactivateBaseCamera;
            _mainCamera = _mainPlayerCamera.GetComponent<CinemachineVirtualCamera>();
            _mainCameraPerlin = _mainCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        }

        private void Update()
        {
            if (_shakeTimer > 0)
            {
                _shakeTimer -= Time.deltaTime;
                if (_shakeTimer <= 0f)
                {
                    _mainCameraPerlin.m_AmplitudeGain = 0f;
                }
            }
        }

        private void ActivateBaseCamera()
        {
            _mainPlayerCamera.SetActive(false);
            _basePlayerCamera.SetActive(true);
        }

        private void DeactivateBaseCamera()
        {
            _mainPlayerCamera.SetActive(true);
            _basePlayerCamera.SetActive(false);
        }

        public void CameraShake(float intensity, float time)
        {
            _mainCameraPerlin.m_AmplitudeGain = intensity;
            _shakeTimer = time;
        }
    }
}