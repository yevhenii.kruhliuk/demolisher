﻿using System;
using Game.Scripts.TriggerZones;
using UnityEngine;

namespace Game.Scripts.Core
{
    public class BaseController : MonoBehaviour
    {
        public Factory Factory
        {
            get => _factory;
            private set => _factory = value;
        }

        public Shop Shop
        {
            get => _shop;
            private set => _shop = value;
        }
        
        public NextLevelTriggerZone NextLevelTriggerZone
        {
            get => _nextLevelTriggerZone;
            private set => _nextLevelTriggerZone = value;
        }

        public GameObject BaseFrontWall
        {
            get => _baseFrontWall;
            private set => _baseFrontWall = value;
        }

        public BaseTriggerZone BaseTriggerZone { get; private set; }

        [SerializeField] private Factory _factory;
        [SerializeField] private Shop _shop;
        [SerializeField] private NextLevelTriggerZone _nextLevelTriggerZone;
        [SerializeField] private GameObject _baseFrontWall;

        private void Awake()
        {
            BaseTriggerZone = GetComponent<BaseTriggerZone>();
        }
    }
}