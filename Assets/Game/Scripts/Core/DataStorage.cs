﻿using System.Collections.Generic;
using Game.Scripts.Controll;
using Game.Scripts.ScriptableObjects;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Core
{
    public class DataStorage : MonoBehaviour
    {
        public static DataStorage Instance { get; private set; }

        [BoxGroup("Demolisher Settings")] public DemolisherStartParams DemolisherStartParams;
        
        [BoxGroup("Buildings Settings")] public BuildParams[] BuildingParams;

        [BoxGroup("Prices")] public UpgradesSetting UpgradesSetting;

        private void Awake()
        {
            if (!Instance)
                Instance = this;
            else
                Destroy(gameObject);    
        }
    }
}