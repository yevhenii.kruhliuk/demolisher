﻿using System;
using System.Collections.Generic;
using AppsFlyerSDK;
using Game.Scripts.Controll;
using Game.Scripts.ObjectPool;
using Game.Scripts.Stucts;
using Game.Scripts.TriggerZones;
using Game.Scripts.UI;
using MoreMountains.NiceVibrations;
using ObjectPool;
using UnityEngine;

namespace Game.Scripts.Core
{
    public class GameManager : MonoBehaviour
    {
        public event Action<int> OnCashAmountChangedEvent;
        public static GameManager Instance { get; private set; }
        public bool IsVibrationActive { get; set; }
        public bool IsAudioActive { get; set; }

        public Demolisher.Demolisher Demolisher;
        public ComponentPool<SomeItem> ItemsPool;
        public Stack<SomeItem> unreceiveItems = new Stack<SomeItem>();
        public BaseController BaseController;
        public CameraManager CameraManager;
        public int CashAmount
        {
            get => _cashAmount;
            set
            {
                _cashAmount = value;
                PlayerPrefs.SetInt(PlayerPrefsKeys.CashAmount, _cashAmount);
                OnCashAmountChangedEvent?.Invoke(_cashAmount);
            }
        }

        public List<Island> Islands;
        public int CurrentIslandNumber;
        public bool CanUseShop;

        [SerializeField] private SomeItem _item;
        [SerializeField] private int _cashAmount;
        private void Awake()
        {
            Initialization();
            if (!Instance)
                Instance = this;
            else
                Destroy(gameObject);
            if (CurrentIslandNumber > 0)
            {
                BaseController.BaseFrontWall.SetActive(true);
                ActivateCurrentIsland();
            }
        }

        private void Start()
        {
            IsVibrationActive = true;
            Dictionary<string, string> eventValues = new Dictionary<string, string>();
            eventValues.Add("LEVEL_NUMBER", UIManager.Instance.InGameMenu._levelCounter.ToString());
            AppsFlyer.sendEvent("LEVEL_STARTED", eventValues);
            EventCaller();
        }

        private void Initialization()
        {
            Application.targetFrameRate = 60;
            Debug.unityLogger.logEnabled = false;
            ItemsPool = ComponentPool<SomeItem>.Get(_item, HideFlags.HideInHierarchy | HideFlags.HideInInspector, true);
            CashAmount = PlayerPrefs.GetInt(PlayerPrefsKeys.CashAmount, 0);
            CurrentIslandNumber = PlayerPrefs.GetInt(PlayerPrefsKeys.CurrentIslandNumber, 0);
            int canUseShop = PlayerPrefs.GetInt(PlayerPrefsKeys.CanUseShop, 0);
            if (canUseShop == 0)
                CanUseShop = false;
            else
                CanUseShop = true;
        }

        private void EventCaller()
        {
            OnCashAmountChangedEvent?.Invoke(CashAmount);
        }
        
        private void ActivateCurrentIsland()
        {
            for (int i = 0; i < Islands.Count; i++)
            {
                if (i == CurrentIslandNumber)
                {
                    Islands[i].gameObject.SetActive(true);
                    Islands[i].MoveIslandOnStart(true, CurrentIslandNumber);
                }
                else
                {
                    Islands[i].gameObject.SetActive(false);
                    Islands[i].MoveIslandOnStart(false, CurrentIslandNumber);
                }
            }
        }
        
        public void ClearUnReceiveItems()
        {
            for (int i = unreceiveItems.Count - 1; i >= 0; i--)
            {
                unreceiveItems.Pop().Put();
            }
        }
        
        public void UseVibration(HapticTypes hapticType)
        {
            if(IsVibrationActive)
                MMVibrationManager.Haptic(hapticType);
        }

        public void SaveData()
        {
            PlayerPrefs.SetInt(PlayerPrefsKeys.CashAmount, CashAmount);
            PlayerPrefs.SetInt(PlayerPrefsKeys.CurrentIslandNumber, CurrentIslandNumber);
        }

        private void OnApplicationQuit()
        {
            SaveData();
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if(!pauseStatus)
                SaveData();
        }
    }
}