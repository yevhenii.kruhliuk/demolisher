﻿using System;
using Game.Scripts.Core;
using Game.Scripts.Stucts;
using UnityEngine;

namespace Game.Scripts.TriggerZones
{
    public class BaseTriggerZone : MonoBehaviour
    {
        public event Action OnComeToBaseEvent;
        public event Action OnLeaveBaseEvent;
        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag(Tags.Player))
                return;
            OnComeToBaseEvent?.Invoke();
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag(Tags.Player))
                return;
            OnLeaveBaseEvent?.Invoke();
        }
    }
}