﻿using System;
using Game.Scripts.Stucts;
using UnityEngine;

namespace Game.Scripts.TriggerZones
{
    public class NextLevelTriggerZone : MonoBehaviour
    {
        public event Action OnNextLevelZoneActivateEvent;
        public event Action OnNextLevelZoneDeactivateEvent;
        private void OnTriggerEnter(Collider other)
        {
            if(!other.CompareTag(Tags.Player))
                return;
            OnNextLevelZoneActivateEvent?.Invoke();
        }
        
        private void OnTriggerExit(Collider other)
        {
            if(!other.gameObject.CompareTag(Tags.Player))
                return;
            OnNextLevelZoneDeactivateEvent?.Invoke();
        }
    }
}