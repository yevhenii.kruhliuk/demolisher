﻿using System;
using Configs.Items;
using Game.Scripts.Core;
using Gameplay.Stack;
using Gameplay.Stack.Interactions;
using Gameplay.Stack.Item;
using Gameplay.Stack.Item.Utils;
using UnityEngine;

namespace Game.Scripts.TriggerZones
{
    public class Factory : MonoBehaviour, IItemReceiver
    {
        public int BlobPrice;
        [SerializeField] private Transform handler;
        public bool Compatible(ItemConfigBase item) => true;

        private void Start()
        {
            BlobPrice = DataStorage.Instance.UpgradesSetting.MoneyPerOneStone;
        }

        public bool Compatible(IItemContainerReadonly container, out ItemConfigBase item)
        {
            item = null;
            foreach (var value in container.Items)
            {
                item = value;
                break;
            }
            return true;
        }

        public Vector3 Position => handler.position;
        public int CanReceiveAmount(ItemConfigBase value) => Int32.MaxValue;

        public bool CanReceive => true;
        public bool AddItem(ItemBase item, bool warp = false)
        {
            AddItemUnsafe(item, warp);
            return true;
        }
        
        private void AddItemUnsafe(ItemBase item, bool warp)
        {
            item.ItemAnim.reachedAnim = ItemAnim.Anim.Disappear;
            item.SetStack(this, Vector3.zero, 1, warp);
        }
        public void ItemStacked(ItemBase item)
        {
            GameManager.Instance.CashAmount += BlobPrice;
        }
        public Transform Container => handler;
    }
}