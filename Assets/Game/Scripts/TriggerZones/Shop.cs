﻿using System;
using System.Collections;
using Game.Scripts.Core;
using Game.Scripts.Stucts;
using UnityEngine;

namespace Game.Scripts.TriggerZones
{
    public class Shop : MonoBehaviour
    {
        public event Action OnShopActivateEvent;
        public event Action OnShopDeactiveEvent;

        public event Action OnFirstTimeUpgrade;
        private void OnTriggerEnter(Collider other)
        {
            if(!other.gameObject.CompareTag(Tags.Player))
                return;
            if(!GameManager.Instance.CanUseShop)
                return;
            StartCoroutine(ScoreTrigger(0.2f));
        }

        private void OnTriggerExit(Collider other)
        {
            if(!other.gameObject.CompareTag(Tags.Player))
                return;
            if(!GameManager.Instance.CanUseShop)
                return;
            OnShopDeactiveEvent?.Invoke();
        }

        private IEnumerator ScoreTrigger(float time)
        {
            yield return new WaitForSecondsRealtime(time);
            if (PlayerPrefs.GetInt(PlayerPrefsKeys.TutorialSecondStep) == 0)
            {
                OnFirstTimeUpgrade?.Invoke();
            }
            OnShopActivateEvent?.Invoke();
        }
    }
}