﻿using UnityEngine;

namespace Utils
{
    public static class Overlaps
    {
        private static readonly Collider[] Colliders = new Collider[128];

        public static int Sphere(in Vector3 position, float radius, out Collider[] colliders)
        {
            colliders = Colliders;
            return Physics.OverlapSphereNonAlloc(position, radius, Colliders);
        }

        public static int Sphere(in Vector3 position, float radius, int layerMask, out Collider[] colliders)
        {
            colliders = Colliders;
            return Physics.OverlapSphereNonAlloc(position, radius, Colliders, layerMask);
        }

        public static int Box(in Vector3 center, in Vector3 halfExtents, in Quaternion orientation, int layerMask, out Collider[] colliders)
        {
            colliders = Colliders;
            return Physics.OverlapBoxNonAlloc(center, halfExtents, colliders, orientation, layerMask);
        }
    }
}