﻿namespace Utils
{
    public interface IUintAccessor<out T>
    {
        public T this[uint id] { get; }
    }
}