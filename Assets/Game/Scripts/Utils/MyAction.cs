﻿namespace Utils
{
    public static class Event<T>
    {
        public delegate void Delegate(in T value);
    }
    public static class Event<T, T1>
    {
        public delegate void Delegate(in T a, in T1 b);
    }
}