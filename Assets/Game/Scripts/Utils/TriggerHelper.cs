﻿using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public abstract class TriggerHelper
    {
        public bool Enabled = true;
        public readonly string Tag;

        protected TriggerHelper(string tag)
        {
            Tag = tag;
        }

        public abstract bool CompareTag(Collider other);
        public abstract void TriggerStay(Collider other);
        public abstract void OnTriggerEnter(Collider other);
        public abstract void OnTriggerExit(Collider other);
        public abstract void Clear();
    }

    public class TriggerHelper<T> : TriggerHelper where T : class
    {
        public delegate void TriggerEvent(T value);

        private readonly Dictionary<Collider, T> values = new Dictionary<Collider, T>();
        public event TriggerEvent OnTrigger;

        public TriggerHelper(string tag, TriggerEvent onTrigger) : base(tag)
        {
            OnTrigger = onTrigger;
        }

        public override bool CompareTag(Collider other)
        {
            return other.CompareTag(Tag);
        }


        protected virtual void Add(Collider other, T value)
        {
            values.Add(other, value);
        }

        protected virtual bool Remove(Collider other)
        {
            return values.Remove(other);
        }

        public sealed override void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent(out T value))
                return;

            Add(other, value);

            if (Enabled)
                Trigger(other, value);
        }

        public sealed override void TriggerStay(Collider other)
        {
            if (!values.TryGetValue(other, out T value))
                return;

            if (Enabled)
                Trigger(other, value);
        }

        public sealed override void OnTriggerExit(Collider other)
        {
            Remove(other);
        }

        protected virtual void Trigger(Collider collider, T value)
        {
            OnTrigger.Invoke(value);
        }

        public override void Clear() => values.Clear();
    }

    public sealed class TriggerHelperInterval<T> : TriggerHelper<T> where T : class
    {
        public readonly float Interval;

        private readonly Dictionary<Collider, float> timing = new Dictionary<Collider, float>();

        public TriggerHelperInterval(string tag, float interval, TriggerEvent onTrigger) : base(tag, onTrigger)
        {
            Interval = interval;
        }

        protected override void Trigger(Collider collider, T value)
        {
            if (Time.time < timing[collider])
                return;

            base.Trigger(collider, value);
            timing[collider] = Time.time + Interval;
        }

        protected override void Add(Collider other, T value)
        {
            base.Add(other, value);
            timing.Add(other, 0);
        }

        protected override bool Remove(Collider other)
        {
            return base.Remove(other) && timing.Remove(other);
        }

        public override void Clear()
        {
            base.Clear();
            timing.Clear();
        }
    }
}