﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Game.Scripts.Core;
using UnityEngine;

namespace Game.Scripts.Controll
{
    public class Island : MonoBehaviour
    {
        public float PercentToComplete;
        
        [SerializeField] private bool _first;
        [SerializeField] private Transform _buildingsParent;
        
        private List<GameObject> _buildings;

        private const float dist = 200f;

        private void Start()
        {
            _buildings = new List<GameObject>();
            for (int i = 0; i < _buildingsParent.childCount; i++)
            {
                _buildings.Add(_buildingsParent.GetChild(i).gameObject);
            }
        }

        public void MoveIsland(bool isCurrent)
        {
            if (isCurrent)
            {
                gameObject.SetActive(true);
                transform.DOMoveX(transform.position.x - dist, 6f).OnComplete(ActivateNewIslandCallback());
            }
            else
            {
                transform.DOMoveX(transform.position.x - dist, 6f).OnComplete(()=>gameObject.SetActive(false));
            }
        }

        public void MoveIslandOnStart(bool isCurrent, float multipler)
        {
            float moveStep = dist * multipler;
            if (isCurrent)
            {
                gameObject.SetActive(true);
                transform.DOMoveX(transform.position.x - moveStep, 3f).OnComplete(ActivateNewIslandCallback());
            }
            else
            {
                transform.DOMoveX(transform.position.x - moveStep, 3f).OnComplete(()=>gameObject.SetActive(false));
            }
        }

        private void ActivateNewIsland()
        {
            if (!_first)
            {
                foreach (var blobZone in _buildings)
                {
                    blobZone.SetActive(true);
                }
            }
            GameManager.Instance.BaseController.BaseFrontWall.SetActive(false);
        }
        
        private TweenCallback ActivateNewIslandCallback()
        {
            TweenCallback callback = () => ActivateNewIsland();
            return callback;
        }
    }
}