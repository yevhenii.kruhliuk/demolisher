﻿using DG.Tweening;
using Game.Scripts.Core;
using Game.Scripts.UI;
using UnityEngine;

namespace Game.Scripts.Controll
{
    public class Movement : MonoBehaviour
    {
        public bool IsMoving { get; private set; }

        [SerializeField] private Transform _wheelBase;
        [SerializeField] private Transform _body;

        private Rigidbody _rigidbody;
        private VariableJoystick _joystick;
        private Demolisher.Demolisher _demolisher;
        private bool _stop;

        private void Awake()
        {
            _demolisher = GetComponent<Demolisher.Demolisher>();
        }

        private void Start()
        {
            IsMoving = false;
            
            _rigidbody = GetComponent<Rigidbody>();
            _joystick = UIManager.Instance.VariableJoystick;

            UIManager.Instance.UpgradeMenu.OnStartMovementEvent += StartMovement;
            UIManager.Instance.UpgradeMenu.OnStopMovementEvent += StopMovement;
        }

        private void FixedUpdate()
        {
            if (!_stop)
            {
                if (!_joystick)
                    _joystick = UIManager.Instance.VariableJoystick;
                _rigidbody.velocity = new Vector3(_joystick.Horizontal * _demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.Speed), _rigidbody.velocity.y,
                    _joystick.Vertical * _demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.Speed));
                if (_joystick.Horizontal != 0 || _joystick.Vertical != 0)
                {
                    _wheelBase.rotation = Quaternion.LookRotation(_rigidbody.velocity);
                    _body.DORotateQuaternion(_wheelBase.rotation, _demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.BodyRotateTime));
                    IsMoving = true;
                }
                else
                {
                    IsMoving = false;
                }
            }
        }

        public void StopMovement()
        {
            _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            _stop = true;
            IsMoving = false;
            _rigidbody.velocity = Vector3.zero;
            _joystick.ResetJS();
        }

        public void StartMovement()
        {
            _rigidbody.constraints = RigidbodyConstraints.None | RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
            _stop = false;
        }

        public float GetCurrentMovespeed()
        {
            return _demolisher.GetDemolisherData(Demolisher.Demolisher.DemolisherDataType.Speed);
        }
    }
}