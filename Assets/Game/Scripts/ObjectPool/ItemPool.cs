﻿using Common;
using Gameplay.Stack.Item;
using ObjectPool;
using UnityEngine;

namespace Game.Scripts.ObjectPool
{
    public class ItemPool : Singleton<ItemPool>
    {
        public ComponentPool<ItemBase> ObjectPool => _objectPool;

        [SerializeField] private ItemBase _itemPrefab;
        private ComponentPool<ItemBase> _objectPool;

        protected override void Init()
        {
            base.Init();
            _objectPool = ComponentPool<ItemBase>.Get(_itemPrefab);
            _objectPool.Fill(100);
        }
    }
}