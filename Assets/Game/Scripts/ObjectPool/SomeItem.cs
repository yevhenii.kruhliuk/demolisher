﻿using System;
using Configs.Items;
using ObjectPool;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Scripts.ObjectPool
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(Rigidbody))]
    
    public class SomeItem : PoolObject
    {
        public ItemConfigBase ItemConfig => _itemConfig;
        public bool ReadyToCollect { get; private set; }

        [HideInInspector] [SerializeField] private MeshFilter _meshFilter;
        [HideInInspector] [SerializeField] private MeshRenderer _meshRenderer;

        [HideInInspector] [SerializeField] private Rigidbody _rigidbody;

        private ItemConfigBase _itemConfig;
        private float _readyToCollectTimer;

        public void Load(ItemConfigBase itemConfig)
        {
            _itemConfig = itemConfig;
            _itemConfig.GetMeshData(out Mesh mesh, out Material[] materials, out float height);
            _meshFilter.mesh = mesh;
            _meshRenderer.materials = materials;
            ReadyToCollect = false;
            _readyToCollectTimer = 1f;
        }

        public void AddExplosionForce()
        {
            Vector3 force = Vector3.up;
            force.z = Random.Range(-1f, 1f);
            force.x = Random.Range(-1f, 1f);
            _rigidbody.AddForce(force * 5000f);
        }

        private void FixedUpdate()
        {
            _readyToCollectTimer -= Time.fixedDeltaTime;
            if (_readyToCollectTimer <= 0)
            {
                ReadyToCollect = true;
            }
        }

        private void Reset()
        {
            _meshFilter = GetComponent<MeshFilter>();
            _meshRenderer = GetComponent<MeshRenderer>();
            _rigidbody = GetComponent<Rigidbody>();
        }
    }
}