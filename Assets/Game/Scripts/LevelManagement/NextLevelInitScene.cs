﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Scripts.LevelManagement
{
    public class NextLevelInitScene : MonoBehaviour
    {
        [SerializeField] private LevelManager _levelManager;
        private void Start()
        {
            _levelManager.LoadLevel();
        }
    }
}