﻿using System.Collections;
using Game.Scripts.Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Scripts.LevelManagement
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] ScriptableObjects.LevelManagerSettings settings;
        //[SerializeField] private GameManager _gameManager; 
        
        private int levelLoadIndex;
        private float loadingProgress;

        private void Awake()
        {
            levelLoadIndex = PlayerPrefs.GetInt("levelIndex", 0);
            /*if (_gameManager != null)
            {
                _gameManager.onWin += () => LevelComplete();
            }*/
        }

        public void LevelComplete()
        {
            levelLoadIndex = settings.levels.IndexOf(SceneManager.GetActiveScene().name);
            if (PlayerPrefs.GetInt("AllLevelsComplete") != 1)
            {
                if (levelLoadIndex + 1 > settings.levelsRange.y)
                {
                    PlayerPrefs.SetInt("levelNum", PlayerPrefs.GetInt("levelNum") + 1);
                    PlayerPrefs.SetInt("LastLevel", levelLoadIndex);
                    PlayerPrefs.SetInt("AllLevelsComplete", 1);
                    int currentLevelIndex = levelLoadIndex;
                    PlayerPrefs.Save();
                    if(settings.levelsRange.x != settings.levelsRange.y)
                    {
                        while (levelLoadIndex == currentLevelIndex || levelLoadIndex == PlayerPrefs.GetInt("LastLevel"))
                        {
                            levelLoadIndex = Random.Range(settings.levelsRange.x, settings.levelsRange.y);
                        }
                    }
                    else
                    {
                        levelLoadIndex = Random.Range(settings.levelsRange.x, settings.levelsRange.y);
                    }
                    
                    PlayerPrefs.SetInt("levelIndex", levelLoadIndex);
                    PlayerPrefs.Save();
                }
                else
                {
                    PlayerPrefs.SetInt("levelNum", PlayerPrefs.GetInt("levelNum") + 1);
                    levelLoadIndex++;
                    PlayerPrefs.SetInt("levelIndex", levelLoadIndex);
                    PlayerPrefs.Save();
                }
            }
            else
            if (PlayerPrefs.GetInt("AllLevelsComplete") == 1)
            {
                PlayerPrefs.SetInt("levelNum", PlayerPrefs.GetInt("levelNum") + 1);
                PlayerPrefs.SetInt("LastLevel", levelLoadIndex);
                int currentLevelIndex = levelLoadIndex;
                PlayerPrefs.Save();
                if (settings.levelsRange.x != settings.levelsRange.y)
                {
                    while (levelLoadIndex == currentLevelIndex || levelLoadIndex == PlayerPrefs.GetInt("LastLevel"))
                    {
                        levelLoadIndex = Random.Range(settings.levelsRange.x, settings.levelsRange.y);
                    }
                }
                else
                {
                    levelLoadIndex = Random.Range(settings.levelsRange.x, settings.levelsRange.y);
                }
                    
                PlayerPrefs.SetInt("levelIndex", levelLoadIndex);
                PlayerPrefs.Save();
            }
        }

        public void LoadLevel()
        {
            SceneManager.LoadScene(settings.levels[levelLoadIndex]);
        }

        public void RestartCurrentLevel()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void LoadLevelAsync()
        {
            StartCoroutine(AsyncLoad(settings.levels[levelLoadIndex]));
        }

        private IEnumerator AsyncLoad(string sceneName)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

            while (!asyncLoad.isDone)
            {
                loadingProgress = asyncLoad.progress;
                yield return null;
            }
        }

        public float GetLoadProgress()
        {
            return loadingProgress;
        }

        public void RestartCurrentLevelAsync()
        {
            Time.timeScale = 1;
            StartCoroutine(AsyncLoad(SceneManager.GetActiveScene().name));
        }


        public int GetLevelNumber()
        {
            int lvlNum = PlayerPrefs.GetInt("levelNum", 1);
            PlayerPrefs.SetInt("levelNum", lvlNum);
            PlayerPrefs.Save();
            return lvlNum;
        }

        public string GetCurrentLevelName()
        {
            return SceneManager.GetActiveScene().name;
        }
    }
}
